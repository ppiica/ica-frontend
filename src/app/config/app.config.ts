import { InjectionToken } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material';

export const APP_CONFIG = new InjectionToken('app.config');


// export const SERVER_URL = 'http://localhost:3000';
export const SERVER_URL = 'http://34.217.75.3:3000';

const API_ROOT = SERVER_URL + '/api/v1';

export const AppConfig = {
  endpoints: {
    technicals: API_ROOT + '/technicals',
    coordinators: API_ROOT + '/coordinators',

    roles: API_ROOT + '/roles',
    categories: API_ROOT + '/categories',
    auth: {
      login: API_ROOT + '/auth/login',
      loggedUser: API_ROOT + '/auth/loggedUser',
      recoverPassword: API_ROOT + '/auth/recoverPassword',
      resetPassword: API_ROOT + '/auth/resetPassword',
      checkTokenToRecoverPassword: API_ROOT + '/auth/checkTokenToResetPassword',
    },
    users: API_ROOT + '/users',
    farms: API_ROOT + '/farms',
    owners: API_ROOT + '/owners',
    regions: {
      departments: API_ROOT + '/regions/departments',
      municipalities: API_ROOT + '/regions/municipalities',
      sidewalks: API_ROOT + '/regions/sidewalks'
    },
    visits: API_ROOT + '/visits',
    offices: API_ROOT + '/offices',
    topten_chart: API_ROOT + '/visits/topten-chart',
    total_visit_chart: API_ROOT + '/visits/status-chart'
  },
  snackBarDuration: 4000,
  tokenKey: 'TOKEN_KEY',
  ROLE_MANAGER: 'MANAGER',
  ROLE_TECHNICIAN: 'TECHNICIAN',
  ROLE_COORDINATOR: 'COORDINATOR',
  permissions: {
    USERS: 'USUARIOS',
    FARMS: 'PREDIOS',
    VISITS: 'VISITAS',
    OWNERS: 'PROPIETARIOS',
    CRUDS: 'MAESTROS',
    USER_ASIGN: 'ASIGNACION_USUARIOS',
    ANY: 'ANY'
  },
  actions: {
    VIEW: 'view',
    CREATE: 'create',
    UPDATE: 'update',
    DELETE: 'delete',
    SHOW_FORM: 'showForm',
  }
};



function spanishRangeLabel(page: number, pageSize: number, length: number) {
  if (length === 0 || pageSize === 0) { return `0 de ${length}`; }

  length = Math.max(length, 0);

  const startIndex = page * pageSize;

  // If the start index exceeds the list length, do not try and fix the end index to the end.
  const endIndex = startIndex < length ?
    Math.min(startIndex + pageSize, length) :
    startIndex + pageSize;

  return `${startIndex + 1} - ${endIndex} de ${length}`;
}


export function getSpanishPaginatorIntl() {
  const paginatorIntl = new MatPaginatorIntl();

  paginatorIntl.itemsPerPageLabel = 'Items por página:';
  paginatorIntl.nextPageLabel = 'Página siguiente';
  paginatorIntl.previousPageLabel = 'Página anterior';
  paginatorIntl.getRangeLabel = spanishRangeLabel;

  return paginatorIntl;
}
