import { AuthGuardService } from './shared/services/authentication/auth-guard.service';
import { AuthenticationService } from './shared/services/authentication/authentication.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MaterialModule } from './material.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { APP_CONFIG, AppConfig, getSpanishPaginatorIntl } from './config/app.config';
import { AppComponent } from './app.component';
import { CoreModule } from './modules/core/core.module';
import { TimingInterceptor } from './shared/interceptors/timing.interceptor';
import { SharedModule } from './shared/shared.module';
import { MatPaginatorIntl, MAT_DATE_LOCALE, MatNativeDateModule } from '@angular/material';
import { TokenInterceptor } from './shared/interceptors/token.interceptor';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    MaterialModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    SharedModule.forRoot(),
    FlexLayoutModule,
    MatNativeDateModule,
  ],
  providers: [
    { provide: APP_CONFIG, useValue: AppConfig },
    { provide: HTTP_INTERCEPTORS, useClass: TimingInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl() },
    {provide: MAT_DATE_LOCALE, useValue: 'es'},
    AuthenticationService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
