export class Technical {
    id: number;
    firstName: string;
    lastNameOne: string;
    lastNameTwo: string;
    professionalRegistration: string;
    coordinatorId: number;
    fullName: string;
}
