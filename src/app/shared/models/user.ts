
export enum USER_TYPES {
  ADMIN = 0,
  COORDINATOR = 1,
  TECHNICIAN = 2
}

export class Permission {
  create: boolean;
  update: boolean;
  delete: boolean;
}

export const DefaultPermissions = {
  create: false,
  update: false,
  delete: false
};

export const UserTypes = [
  {
    id: USER_TYPES.ADMIN,
    name: USER_TYPES[0]
  },
  {
    id: USER_TYPES.COORDINATOR,
    name: USER_TYPES[1]
  },
  {
    id: USER_TYPES.TECHNICIAN,
    name: USER_TYPES[2]
  }
];

export class User {

  public id?: number;
  public email?: string;
  public createdAt?: Date;
  public updatedAt?: Date;
  public roleId?: number;
  public techniciansIds?: Array<string>;
  public userType?: { name: string };
  public profile: {
    firstName: string;
    lastNameOne?: string;
    lastNameTwo?: string;
    officeId?: string;
    professionalRegistration?: string;
  };
  public UserRoleForm?: {
    create: boolean,
    update: boolean,
    delete: boolean,
    formId: number,
    form: {
      description: string
    }
  }[];
  public password?: string;
}
