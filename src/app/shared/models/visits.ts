import { Farm } from './farms';
import { Qualification } from './qualification';
import { Technical } from './technical';

export class Visit {

  public id?: number;
  public date: string;
  public nextVisitDate?: string;
  public observations?: string;
  public gradualCompliancePlan?: string;
  public ownerFarmSign?: string;
  public inspectorSign?: string;
  public farmRegisterNumber: number;
  public statusId?: number;
  public technicalId: number;
  public createdAt?: Date;
  public updatedAt?: Date;
  public isRemove?: boolean;
  public status?: {
    description: string
  };
  public productiveActivities?: {
    breed: number,
    lift: number,
    fatten: number,
    fullCycle: number,
    apply: boolean,
    specie: {
      description: string
    }
  }[];
  public qualifications?: Qualification[];
  public technical?: Technical;
  public farm?: Farm;
  public personWhoAttendsId?: number;
  public personWhoAttendsName?: string;
  public personWhoAttendsPhone?: number;
}
