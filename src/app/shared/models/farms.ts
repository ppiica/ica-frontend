import { Owner } from './owners';

export class Farm {

  public registerNumber: number;
  public name: string;
  public latitude?: number;
  public longitude?: number;
  public area?: number;
  public sidewalkId: string;
  public ownerId: string;
  public createdAt?: Date;
  public officeId: string;
  public updatedAt?: Date;
  public sidewalk?: Sidewalk;
  public owner?: Owner;
}

class Sidewalk {
  public id: number;
  public name: string;
  public municipality?: Municipality;
}

class Municipality {
  public id: number;
  public name: string;
  public department?: Department;
}

class Department {
  public id: number;
  public name: string;
}
