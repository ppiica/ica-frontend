export class Qualification {

  public idEvaluation: string;
  public score: number;
  public observations: string;
  public evidence: string;
  public createdAt?: Date;
  public updatedAt?: Date;
  public password: string;
  public itemId: number;

}
