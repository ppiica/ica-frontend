import { Qualification } from './qualification';
export class Category {

  public id: number;
  public description: string;
  public maxScore: number;
  public categoryPercent: number;
  public items: Item[];

}

class Item {
  public id: number;
  public description: string;
  public qualifications: Qualification[];
}

export class CategoryResume {
  categoryId: number;
  categoryDescription: string;
  itemsNA: number;
  itemsSucceed: number;
  percent: string;
}
