
export class Owner {

      public id?: string;
      public firstName?: string;
      public lastNameOne?: string;
      public lastNameTwo: string;
      public phone?: number;
      public email?: string;
      public address?: string;
      public fullName?: string;

    }
