import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from './../services/authentication/authentication.service';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler } from '@angular/common/http';
import { LoggerService } from '../services/logger/logger.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    private authenticationService: AuthenticationService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.authenticationService.getToken();
    LoggerService.log(token);
    if (token) {
      req = req.clone({
        setHeaders: {
          token: token
        }
      });
    }
    return next.handle(req);
  }
}
