import { UserTypesService } from './services/user-types/user-types.service';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeleteDialogComponent } from './components/delete-dialog/delete-dialog.component';
import { MaterialModule } from '../material.module';
import { TranslateModule } from '@ngx-translate/core';
import { UtilService } from './services/util/util.service';
import { RegionsService } from './services/regions/regions.service';
import { OwnersService } from '../modules/owners/owners.service';
import { OfficesService } from '../modules/offices/shared/offices.service';

@NgModule({
  declarations: [
    DeleteDialogComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    TranslateModule.forChild()
  ],
  exports: [
  ],
  providers: [
  ],
  entryComponents: [
    DeleteDialogComponent
  ]
})
export class SharedModule {

  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        UtilService,
        RegionsService,
        UserTypesService,
        OfficesService,
        OwnersService
      ]
    };
  }

}
