import { catchError } from 'rxjs/operators';
import { UtilService } from './../util/util.service';
import { Technical } from './../../models/technical';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from './../../../config/app.config';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class TechnicalsService {

  constructor(
    private http: HttpClient,
    private utilsService: UtilService
  ) {
  }

  public getTechnicals(): Observable<Technical[]> {
    return this.http.get<Technical[]>(AppConfig.endpoints.technicals)
    .pipe(
      catchError(this.utilsService.handleError<Technical[]>('get technicals'))
    );
  }

}
