import { catchError, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { AppConfig } from '../../../config/app.config';
import { UtilService } from './../util/util.service';
import { LoggerService } from '../logger/logger.service';
import { Region } from '../../models/regions';


@Injectable()
export class RegionsService {

  constructor(
    private http: HttpClient,
    private utils: UtilService
  ) { }

  public getDepartments(): Observable<Region[]> {
    return this.http.get<Region[]>(AppConfig.endpoints.regions.departments)
      .pipe(
        catchError(this.utils.handleError<Region[]>('get departments'))
      );
  }

  public getMunicipalities(departmentId: number): Observable<Region[]> {
    return this.http.get<Region[]>(AppConfig.endpoints.regions.municipalities + '/' + departmentId)
      .pipe(
        catchError(this.utils.handleError<Region[]>('get departments'))
      );
  }

  public getSidewalks(municipalityId: number): Observable<Region[]> {
    return this.http.get<Region[]>(AppConfig.endpoints.regions.sidewalks + '/' + municipalityId)
      .pipe(
        catchError(this.utils.handleError<Region[]>('get departments'))
      );
  }



}
