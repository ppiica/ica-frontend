import { catchError } from 'rxjs/operators';
import { UtilService } from './../util/util.service';
import { Technical } from './../../models/technical';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from './../../../config/app.config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../models/user';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CoordinatorService {

    public coordinatorToAssign: User;

    constructor(
        private http: HttpClient,
        private utilsService: UtilService
    ) {
    }

    public assignTechnicals(coordinatorId: number, coordinator: User): Promise<any> {
        return this.http.post<any>(AppConfig.endpoints.coordinators + '/' + coordinatorId + '/technicals', coordinator, httpOptions).toPromise();
    }

    public getTechnicalsByCoordinator(coordinatorId: number): Observable<Technical[]> {
        return this.http.get<Technical[]>(AppConfig.endpoints.coordinators + '/' + coordinatorId + '/technicals')
            .pipe(
                catchError(this.utilsService.handleError<Technical[]>('get technicals by coordinator'))
            );
    }
}