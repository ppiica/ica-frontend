import { Category } from './../../models/category';
import { of } from 'rxjs/observable/of';
import { UtilService } from './../util/util.service';
import { LoggerService } from './../logger/logger.service';
import { catchError } from 'rxjs/operators';
import { AppConfig } from './../../../config/app.config';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CategoriesService {

  constructor(
    private http: HttpClient,
    private utils: UtilService
  ) { }

  public getAllCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(AppConfig.endpoints.categories)
    .pipe(catchError(this.handleError<Category[]>('get categories')));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      LoggerService.log(`${operation} failed: ${error.message}`);

      switch (error.status) {
        case 500:
          this.utils.showSnackBar('ERROR_500');
          break;
        default:
          break;
      }


      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
