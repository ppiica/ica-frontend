import { of } from 'rxjs/observable/of';
import { Observable } from 'rxjs/Observable';
import { LoggerService } from './../logger/logger.service';
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from '../../../config/app.config';
import { AbstractControl } from '@angular/forms';

@Injectable()
export class UtilService {

  public OK_TEXT: any;

  constructor(
    private matSnackBar: MatSnackBar,
    private translate: TranslateService
  ) {
    this.translate.get(['OK']).subscribe((values) => {
      this.OK_TEXT = values.OK;
    });
  }

  public showSnackBar(name: string, data?: any): void {
    const config: any = new MatSnackBarConfig();
    config.duration = AppConfig.snackBarDuration;
    this.translate.get(name).subscribe((value: string) => {
      this.matSnackBar.open(value, this.OK_TEXT, config);
    });
  }

  public MatchPassword(abstractControl: AbstractControl) {
    const password = abstractControl.get('password').value; // to get value in input tag
    const confirmPassword = abstractControl.get('confirmPassword').value; // to get value in input tag
    if (password !== confirmPassword) {
      console.log('false');
      abstractControl.get('confirmPassword').setErrors({ MatchPassword: true });
    } else {
      console.log('true');
      return null;
    }
  }

  public handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      LoggerService.log(`${operation} failed: ${error.message}`);

      switch (error.status) {
        case 500:
          this.showSnackBar('ERROR_500');
          break;
        default:
          break;
      }


      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
