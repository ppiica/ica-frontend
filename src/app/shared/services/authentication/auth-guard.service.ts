import { AppConfig } from './../../../config/app.config';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from './../../../shared/services/authentication/authentication.service';
import { User } from './../../../shared/models/user';
import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { LoggerService } from '../logger/logger.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    const viewData = route.data as {
      formName: string;
      action: string;
    };
    if (viewData.formName && viewData.action) {
      return this.validate(viewData);
    } else {
      return true;
    }
  }

  private getLoginPath(): Array<string> {
    return ['login'];
  }

  private getDashobardPath(): Array<string> {
    return ['admin', 'dashboard'];
  }

  private validateAction(action: string, permission: any): boolean {
    let canContinue;
    switch (action) {
      case AppConfig.actions.CREATE:
        canContinue = permission.create;
        break;
      case AppConfig.actions.UPDATE:
        canContinue = permission.update;
        break;
      case AppConfig.actions.DELETE:
        canContinue = permission.delete;
        break;
      case AppConfig.actions.VIEW:
        canContinue = true;
        break;
      case AppConfig.actions.SHOW_FORM:
        canContinue = permission.update || permission.create;
        break;
    }
    return canContinue;
  }

  private async validate({ formName, action }): Promise<boolean> {
    // tslint:disable-next-line:prefer-const
    let user: User = null;
    let canContinue = false;
    try {
      user = (await this.authenticationService.getLoggedUser()).value;
      if (user) {
        if (formName === AppConfig.permissions.ANY) {
          canContinue = true;
        } else {
          const permission = user.UserRoleForm.find(
            p => p.form.description === formName
          );
          if (permission) {
            canContinue = this.validateAction(action, permission);
          }
        }
      }
      console.log('user', user);
    } catch (exception) {
      console.log(exception);
      LoggerService.log(exception);
    }
    if (!canContinue) {
      const path = user ? this.getDashobardPath() : this.getLoginPath();
      this.router.navigate(path);
    }
    LoggerService.log(JSON.stringify(user));
    console.log({ formName, action, canContinue });
    return canContinue;
  }
}
