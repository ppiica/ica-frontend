import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { async } from '@angular/core/testing';
import { User, DefaultPermissions } from './../../models/user';
import { AppConfig } from './../../../config/app.config';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { utils, promise } from 'protractor';
import { LoggerService } from '../logger/logger.service';

@Injectable()
export class AuthenticationService {

  private userInfo: User = null;
  private readonly userBS: BehaviorSubject<User>;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.userBS = new BehaviorSubject<User>(this.userInfo);
  }

  private setUser(userInfo: User) {
    this.userInfo = userInfo;
    this.userBS.next(this.getUserCopy());
  }

  private getUserCopy(): User {
    return this.userInfo ? JSON.parse(JSON.stringify(this.userInfo)) : null;
  }


  public async login(email: string, password: string): Promise<User> {
    const data = {
      email,
      password
    };
    try {
      const tokenData: { token: string, user: User } = await this.http.post(AppConfig.endpoints.auth.login, data).toPromise<any>();
      localStorage.setItem(AppConfig.tokenKey, tokenData.token);
      this.setUser(tokenData.user);
      return this.getUserCopy();
    } catch (error) {
      LoggerService.error(error);
      return Promise.reject(error);
    }
  }

  public logout() {
    localStorage.removeItem(AppConfig.tokenKey);
    this.router.navigate(['login', 'login']);
  }

  public getToken(): string {
    return localStorage.getItem(AppConfig.tokenKey);
  }

  public validatePermission(formName: string) {
    if (this.userInfo && this.userInfo.UserRoleForm) {
      const permissions = this.userInfo.UserRoleForm.find(p => p.form.description === formName);
      if (permissions) {
        return {
          create: permissions.create,
          update: permissions.update,
          delete: permissions.delete
        };
      }
    } else {
      return DefaultPermissions;
    }
  }

  public async getLoggedUser(): Promise<BehaviorSubject<User>> {
    if (!this.userInfo) {
      try {
        this.setUser(await this.http.get<User>(AppConfig.endpoints.auth.loggedUser).toPromise());
      } catch (exception) {
        this.setUser(null);
        return Promise.reject(exception);
      }
      if (!this.userInfo) {
        this.setUser(null);
        return Promise.reject({ msg: 'USER_NOT_FOUND' });
      }
    }
    return this.userBS;
  }

  public recoverPassword(email: string): Promise<any> {
    return this.http.post<any>(AppConfig.endpoints.auth.recoverPassword, { email }).toPromise();
  }

  public checkTokenToRecoverPassword(token: string): Promise<User> {
    return this.http.post<User>(AppConfig.endpoints.auth.checkTokenToRecoverPassword, { token }).toPromise();
  }

  public resetPassword(token: string, password: string): Promise<User> {
    return this.http.put<any>(AppConfig.endpoints.auth.resetPassword, { token, password }).toPromise();
  }

}
