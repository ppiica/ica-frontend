import { AppConfig } from './../../../config/app.config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Injectable } from '@angular/core';

import { UserType } from './../../models/user-types';

@Injectable()
export class UserTypesService {



  constructor(
    private http: HttpClient
  ) {

  }

  public async getRoles(): Promise<UserType[]> {
    return await this.http.get<UserType[]>(AppConfig.endpoints.roles).toPromise();
  }

  /**
   * getUserTypes
   */

}
