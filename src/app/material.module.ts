import { NgModule } from '@angular/core';

import {
  MatButtonModule,
  MatIconModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule,
  MatListModule,
  MatGridListModule,
  MatTableModule,
  MatPaginatorModule,
  MatFormFieldModule,
  MatCardModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatInputModule,
  MatDialogModule,
  MatSnackBarModule,
  MatSelectModule,
  MatSortModule,
  MatDatepickerModule,
  MatTooltipModule,
  MatCheckboxModule
} from '@angular/material';

const materialImports = [
  MatButtonModule,
  MatIconModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule,
  MatListModule,
  MatGridListModule,
  MatTableModule,
  MatPaginatorModule,
  MatFormFieldModule,
  MatCardModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatInputModule,
  MatDialogModule,
  MatSnackBarModule,
  MatSelectModule,
  MatSortModule,
  MatDatepickerModule,
  MatTooltipModule,
  MatCheckboxModule
];

@NgModule({
  imports: materialImports,
  exports: materialImports,
})
export class MaterialModule { }
