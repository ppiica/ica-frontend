import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitFormPageComponent } from './visit-form.page';

describe('VisitFormComponent', () => {
  let component: VisitFormPageComponent;
  let fixture: ComponentFixture<VisitFormPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitFormPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitFormPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
