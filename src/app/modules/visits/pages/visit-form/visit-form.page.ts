import { FarmsService } from './../../../farms/shared/farms.service';
import { Farm } from './../../../../shared/models/farms';
import { Observable } from 'rxjs/Observable';
import { LoggerService } from './../../../../shared/services/logger/logger.service';
import { Technical } from './../../../../shared/models/technical';
import { TechnicalsService } from './../../../../shared/services/techincals/techicals.service';
import { AppConfig } from './../../../../config/app.config';
import { AuthenticationService } from './../../../../shared/services/authentication/authentication.service';
import { Subscription } from 'rxjs/Subscription';
import { Permission, DefaultPermissions, User } from './../../../../shared/models/user';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Visit } from '../../../../shared/models/visits';
import { VisitsService } from '../../shared/visits.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-visit-page-form',
  templateUrl: './visit-form.page.html',
  styleUrls: ['./visit-form.page.css']
})
export class VisitFormPageComponent implements OnInit, OnDestroy {

  public mode = 'edit';
  public visitData: Visit;
  public visitsPermission$: BehaviorSubject<Permission>;
  private userSubscription: Subscription;
  public technicalsObservable: Observable<Technical[]>;
  public farmsObservable: Observable<Farm[]>;

  constructor(
    private visitsService: VisitsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private technicalsService: TechnicalsService,
    private farmsService: FarmsService
  ) {
    this.visitsPermission$ = new BehaviorSubject(DefaultPermissions);
  }

  ngOnInit() {
    this.getVisit();
    this.validateActions();
    this.getTechnicals();
    this.getFarms();
  }

  ngOnDestroy() {
    delete this.visitsService.visitToEdit;
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }

  public createVisit(visit: Visit) {
    this.visitsService.createVisit(visit).subscribe((res) => {
      this.router.navigate(['admin', 'visits', 'list']);
    });
  }

  public updateVisit(visit: Visit) {
    this.visitsService.updateVisit(visit).subscribe((res) => {
      this.router.navigate(['admin', 'visits', 'list']);
    });
  }

  private getVisit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params.mode) {
        this.mode = params.mode;
      }
      if (this.visitsService.visitToEdit) {
        this.visitData = this.visitsService.visitToEdit;
      } else if (this.mode !== 'create' && params.id ) {
        this.visitsService.getVisit(params.id).subscribe((visit) => {
          this.visitData = visit;
        });
      }
    });
  }

  private onUserChange(user: User) {
    this.visitsPermission$.next(this.authenticationService.validatePermission(AppConfig.permissions.VISITS));
  }

  private async validateActions() {
    if (!this.userSubscription) {
      this.userSubscription = (await this.authenticationService.getLoggedUser()).subscribe(((user: User) => this.onUserChange(user)));
    }
  }

  private getTechnicals() {
    this.technicalsObservable = this.technicalsService.getTechnicals();
  }

  private getFarms() {
    this.farmsObservable = this.farmsService.getFarms();
  }

}
