import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import { Permission, DefaultPermissions } from './../../../../shared/models/user';
import { AppConfig } from './../../../../config/app.config';
import { AuthenticationService } from './../../../../shared/services/authentication/authentication.service';
import { MatDialog } from '@angular/material';
import { Visit } from './../../../../shared/models/visits';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { VisitsService } from '../../shared/visits.service';
import { Router } from '@angular/router';
import { DeleteDialogComponent } from '../../../../shared/components/delete-dialog/delete-dialog.component';
import { User } from '../../../../shared/models/user';

@Component({
  selector: 'app-visits',
  templateUrl: './visits.page.html',
  styleUrls: ['./visits.page.css']
})
export class VisitsPageComponent implements OnInit, OnDestroy {
  public visits: Observable<Visit[]>;
  public canCreate = false;
  public visitsPermission$: BehaviorSubject<Permission>;
  private userSubscription: Subscription;

  constructor(
    private visitsService: VisitsService,
    private router: Router,
    private matDialog: MatDialog,
    private authenticationService: AuthenticationService
  ) {
    this.visitsPermission$ = new BehaviorSubject(DefaultPermissions);
  }

  ngOnInit() {
    this.validateActions();
    this.getVisits();
  }

  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }

  private onUserChange(user: User) {
    this.visitsPermission$.next(this.authenticationService.validatePermission(AppConfig.permissions.VISITS));
  }

  private async validateActions() {
    if (!this.userSubscription) {
      this.userSubscription = (await this.authenticationService.getLoggedUser()).subscribe((user: User) => this.onUserChange(user));
    }
  }

  public editVisit(visit: Visit) {
    this.visitsService.visitToEdit = visit;
    console.log(visit);
    this.router.navigate(['admin', 'visits', 'form', visit.id]);
  }

  public removeVisit(visitId: string) {
    const dialog = this.matDialog.open(DeleteDialogComponent, {
      data: {
        removeItem: 'REMOVE_VISIT'
      }
    });
    dialog.beforeClose().subscribe(result => {
      if (result) {
        this.visitsService.removeVisit(visitId).subscribe();
      }
    });
  }

  public getVisits() {
    this.visits = this.visitsService.getVisits();
  }

  public detailVisit(visit: Visit) {
    this.visitsService.visitToEdit = visit;
    this.router.navigate(['admin', 'visits', 'summary', visit.id]);
  }
}
