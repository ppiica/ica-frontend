import { AppConfig } from './../../config/app.config';
import { AuthGuardService } from './../../shared/services/authentication/auth-guard.service';
import { VisitFormPageComponent } from './pages/visit-form/visit-form.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VisitsPageComponent } from './pages/visits/visits.page';
import { VisitSummaryFormPageComponent } from './pages/visits-summary/visits-summary.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
    data: {formName: AppConfig.permissions.VISITS , action: AppConfig.actions.VIEW }
  },
  {
    path: 'list',
    component: VisitsPageComponent,
    canActivate: [AuthGuardService],
    data: {formName: AppConfig.permissions.VISITS , action: AppConfig.actions.VIEW }
  },
  {
    path: 'form/:id',
    component: VisitFormPageComponent,
    canActivate: [AuthGuardService],
    data: {formName: AppConfig.permissions.VISITS , action: AppConfig.actions.SHOW_FORM }
  },
  {
    path: 'summary/:id',
    component: VisitSummaryFormPageComponent,
    canActivate: [AuthGuardService],
    data: {formName: AppConfig.permissions.VISITS , action: AppConfig.actions.VIEW }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VisitsRoutingModule { }

export const routedComponents = [VisitsPageComponent,VisitSummaryFormPageComponent];
