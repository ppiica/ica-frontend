import { Farm } from './../../../../shared/models/farms';
import { Observable } from 'rxjs/Observable';
import { Technical } from './../../../../shared/models/technical';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Permission } from './../../../../shared/models/user';
import { Subscription } from 'rxjs/Subscription';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Visit } from '../../../../shared/models/visits';

@Component({
  selector: 'app-visit-form',
  templateUrl: './visit-form.component.html',
  styleUrls: ['./visit-form.component.css']
})
export class VisitFormComponent implements OnInit, OnDestroy {

  public visitForm: FormGroup;
  public title: string;
  public startDate = new Date();
  private visitsSubscription: Subscription;
  private technicalsSubscription: Subscription;
  private farmsSubscription: Subscription;
  public visitsPermissions: Permission;
  public technicals: Technical[];
  public farms: Farm[];

  @Input()
  public mode: string;

  @Input()
  public visit: Visit;

  @Input()
  public visitsPermission$: BehaviorSubject<Permission>;

  @Input()
  public technicalsObservable: Observable<Technical[]>;

  @Input()
  public farmsObservable: Observable<Farm[]>;

  @Output()
  public submitCreate: EventEmitter<Visit>;

  @Output()
  public submitUpdate: EventEmitter<Visit>;

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.submitCreate = new EventEmitter();
    this.submitUpdate = new EventEmitter();
  }

  ngOnInit() {
    this.initForm(this.visit);
    this.validateActions();
    this.loadTechnicals();
    this.loadFarms();
  }

  ngOnDestroy() {
    this.unsubscribe(this.visitsSubscription);
    this.unsubscribe(this.technicalsSubscription);
  }

  private unsubscribe(subscription: Subscription) {
    if (subscription) {
      subscription.unsubscribe();
    }
  }

  private initForm(visit?: Visit) {
    this.visitForm = this.formBuilder.group({
      technician: [undefined, Validators.required],
      farm: [undefined, Validators.required],
      date: [new Date(), Validators.required]
    });

    if (this.mode === 'edit') {
      // this.visitForm.
    }

    if (visit) {
      const visitSet = {
        technician: visit.technicalId,
        farm: visit.farmRegisterNumber,
        date: new Date(visit.date)
      };
      this.visitForm.setValue(visitSet);
    }
  }

  public submit() {
    const visit: Visit = this.prepareDataToSend();
    if (this.mode === 'create') {
      this.submitCreate.emit(visit);
    } else if (this.mode === 'edit') {
      this.submitUpdate.emit(visit);
    }
  }

  private validateActions() {
    if (!this.visitsSubscription) {
      this.visitsSubscription = this.visitsPermission$.subscribe((permission: Permission) => {
        this.visitsPermissions = permission;
      });
    }
  }

  private loadTechnicals() {
    if (!this.technicalsSubscription) {
      this.technicalsSubscription = this.technicalsObservable.subscribe((technicals: Technical[]) => {
        this.technicals = technicals;
        console.log(technicals);
      });
    }
  }

  private loadFarms() {
    if (!this.farmsSubscription) {
      this.farmsSubscription = this.farmsObservable.subscribe((farms: Farm[]) => {
        console.log(farms);
        this.farms = farms;
      });
    }
  }

  private prepareDataToSend(): Visit {
    const formValue = this.visitForm.value;
    const visit: Visit = {
      date: formValue.date.toISOString(),
      farmRegisterNumber: formValue.farm,
      technicalId: formValue.technician.toString()
    };
    if (this.visit) {
      visit.id = this.visit.id;
    }
    return visit;
  }

}
