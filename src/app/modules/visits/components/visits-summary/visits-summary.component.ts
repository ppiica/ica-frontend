import { SERVER_URL } from './../../../../config/app.config';
import { Category, CategoryResume } from './../../../../shared/models/category';
import { Farm } from './../../../../shared/models/farms';
import { Observable } from 'rxjs/Observable';
import { Technical } from './../../../../shared/models/technical';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Permission } from './../../../../shared/models/user';
import { Subscription } from 'rxjs/Subscription';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Visit } from '../../../../shared/models/visits';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-visits-summary-form',
  templateUrl: './visits-summary.component.html',
  styleUrls: ['./visits-summary.component.css']
})
export class VisitSummaryFormComponent implements OnInit, OnDestroy {

  public title: string;
  public startDate = new Date();
  private visitsSubscription: Subscription;
  private categoriesSubscription: Subscription;
  public visitsPermissions: Permission;
  public categories: Category[];
  public data;
  public categoriesResume: CategoryResume[];
  public totalNA = 0;
  public totalSucceed = 0;
  public totalPercent = 0;
  public readonly apiRoot = SERVER_URL;
  public evidences: any[] = [];

  @Input()
  public mode: string;

  @Input()
  public visit: Visit;

  @Input()
  public visitsPermission$: BehaviorSubject<Permission>;

  @Input()
  public categoriesObservable: Observable<Category[]>;

  @Output()
  public submitCreate: EventEmitter<Visit>;

  @Output()
  public submitUpdate: EventEmitter<Visit>;

  constructor(
    public datepipe: DatePipe

  ) {
    this.submitCreate = new EventEmitter();
    this.submitUpdate = new EventEmitter();
    this.data = new Visit();
  }
  ngOnInit() {
    console.log(this.visit);
    this.initData(this.visit);
    this.validateActions();
    this.loadCategories();
  }

  ngOnDestroy() {
    this.unsubscribe(this.visitsSubscription);
    this.unsubscribe(this.categoriesSubscription);
  }

  private unsubscribe(subscription: Subscription) {
    if (subscription) {
      subscription.unsubscribe();
    }
  }

  private initData(visit?: Visit) {
    if (visit) {
      this.data = this.visit;
      this.data.date = this.datepipe.transform(this.data.date, 'dd/MM/yyyy');
      this.data.nextVisitDate = this.datepipe.transform(this.data.nextVisitDate, 'dd/MM/yyyy');
      // this.data.productiveActivities = this.visit.productiveActivities
      //                                 .concat(this.visit.productiveActivities)
      //                                 .concat(this.visit.productiveActivities)
      //                                 .concat(this.visit.productiveActivities)
      //                                 .concat(this.visit.productiveActivities)
      //                                 .concat(this.visit.productiveActivities)
      //                                 .concat(this.visit.productiveActivities)
      //                                 .concat(this.visit.productiveActivities);
    } else {
      this.data = new Visit();
      this.data.id = '';
      this.data.date = '';
      this.data.nextVisitDate = '';
      this.data.farm = new Array();
      this.data.farm.name = '';
      this.data.farm.sidewalk = new Array();
      this.data.farm.sidewalk.name = '';
      this.data.farm.sidewalk.municipality = new Array();
      this.data.farm.sidewalk.municipality.name = '';
      this.data.farm.sidewalk.municipality.department = new Array();
      this.data.farm.sidewalk.municipality.department.name = '';
      this.data.farm.registerNumber = '';
      this.data.farm.latitude = '';
      this.data.farm.longitude = '';
      this.data.farm.owner = new Array();
      this.data.farm.owner.firstName = '';
      this.data.farm.owner.lastNameOne = '';
      this.data.farm.owner.id = '';
      this.data.farm.owner.phone = '';
      this.data.farm.owner.email = '';
      this.data.farm.owner.address = '';
      this.data.personWhoAttendsName = '';
      this.data.personWhoAttendsId = '';
      this.data.personWhoAttendsPhone = '';
      this.data.technical = new Array();
      this.data.technical.firstName = '';
      this.data.technical.lastNameOne = '';
      this.data.technical.professionalRegistration = '';
      this.data.productiveActivities = [];
      this.data.qualifications = [];
      this.data.ownerFarmSign = '';
      this.data.inspectorSign = '';


    }
  }

  private validateActions() {
    if (!this.visitsSubscription) {
      this.visitsSubscription = this.visitsPermission$.subscribe((permission: Permission) => {
        this.visitsPermissions = permission;
      });
    }
  }


  private loadCategories() {
    if (!this.categoriesSubscription) {
      this.categoriesSubscription = this.categoriesObservable.subscribe((categories: Category[]) => {
        this.categories = categories;
        this.categoriesResume = [];
        this.evidences = [];
        this.totalNA = 0;
        this.totalSucceed = 0;
        let totalPercent = 0;
        this.categories.forEach((category: Category) => {
          let categoryItemsSucceed = 0;
          let categoryItemsNA = 0;
          let totalEvaluatedCategory = 0;
          category.items.forEach((item) => {
            item.qualifications = [];
            if (this.visit) {
              this.visit.qualifications.forEach(qualification => {
                if (qualification.itemId === item.id) {
                  item.qualifications.push(qualification);
                  totalEvaluatedCategory += qualification.score;
                  if (qualification.score >= (category.maxScore * 0.6)) {
                    categoryItemsSucceed++;
                  }
                  if (qualification.evidence) {
                    this.evidences.push({item : qualification.itemId, url: qualification.evidence.replace('./public', '')});
                  }
                }
              });
            }
            if (!item.qualifications.length) {
              categoryItemsNA++;
            }
          });
          const categoryResume: CategoryResume = {
            categoryId: category.id,
            categoryDescription: category.description,
            itemsNA: categoryItemsNA,
            itemsSucceed: categoryItemsSucceed,
            percent: 'N/A'
          };
          if (categoryItemsNA < category.items.length) {
            const evaluatedItems = category.items.length - categoryItemsNA;
            const categoryResumePercent = (totalEvaluatedCategory * 100) / (evaluatedItems * category.maxScore);
            categoryResume.percent = categoryResumePercent.toFixed(2) + '%';
            this.totalPercent += (categoryResumePercent * category.categoryPercent);
            totalPercent += category.categoryPercent;
          }
          this.totalNA += categoryItemsNA;
          this.totalSucceed += categoryItemsSucceed;

          this.categoriesResume.push(categoryResume);
        });
        this.totalPercent = this.totalPercent / totalPercent;
        console.log(categories);
      });
    }
  }

}
