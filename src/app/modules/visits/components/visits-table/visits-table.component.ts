import { Permission } from './../../../../shared/models/user';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Visit } from '../../../../shared/models/visits';
import { Subscription } from 'rxjs/Subscription';
import { } from 'events';

@Component({
  selector: 'app-visits-table',
  templateUrl: './visits-table.component.html',
  styleUrls: ['./visits-table.component.css']
})
export class VisitsTableComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  @ViewChild('input') input: ElementRef;

  public visitsDataSource: MatTableDataSource<Visit>;

  public visitsPermissions: Permission;

  private visitsSubscription: Subscription;

  public displayedColumns = ['farm', 'technician', 'date', 'actions'];

  @Input()
  public visitsObservable: Observable<Visit[]>;

  @Input()
  public visitsPermission$: BehaviorSubject<Permission>;

  @Output()
  public clickEditVisit: EventEmitter<Visit>;

  @Output()
  public clickRemoveVisit: EventEmitter<Visit>;

  @Output()
  public clickDetailVisit: EventEmitter<Visit>;

  constructor() {
    this.clickEditVisit = new EventEmitter();
    this.clickRemoveVisit = new EventEmitter();
    this.clickDetailVisit = new EventEmitter();
  }

  ngOnInit() {
    this.visitsDataSource = new MatTableDataSource([]);
    this.validatePermissions();
    this.loadData();
  }

  ngAfterViewInit() {
    this.visitsDataSource.paginator = this.paginator;
    this.visitsDataSource.sort = this.sort;
  }

  ngOnDestroy() {
    if (this.visitsSubscription) {
      this.visitsSubscription.unsubscribe();
    }
  }

  public applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.visitsDataSource.filter = filterValue;
  }

  private loadData() {
    this.visitsSubscription = this.visitsObservable.subscribe((visits) => {
      console.log(visits);
      this.visitsDataSource.data = visits;
    });
  }

  private validatePermissions() {
    if (!this.visitsPermissions) {
      this.visitsSubscription = this.visitsPermission$.subscribe((permissions: Permission) => {
        this.visitsPermissions = permissions;
      });
    }
  }

}
