import { Visit } from './../../../shared/models/visits';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

import { LoggerService } from '../../../shared/services/logger/logger.service';
import { UtilService } from '../../../shared/services/util/util.service';
import { AppConfig } from '../../../config/app.config';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class VisitsService {

  public visits$: BehaviorSubject<Visit[]>;
  public visitToEdit: Visit;

  constructor(
    private http: HttpClient,
    private utils: UtilService
  ) {
    this.visits$ = new BehaviorSubject<Visit[]>([]);
  }

  public async getVisitApprovedChart(): Promise<any[]> {
    return this.http.get<any[]>(AppConfig.endpoints.topten_chart).toPromise();
  }

  public async getTotalVisitByStateChart(): Promise<any[]> {
    return this.http.get<any[]>(AppConfig.endpoints.total_visit_chart).toPromise();
  }

  public getVisits(): Observable<Visit[]> {
    this.http.get<Visit[]>(AppConfig.endpoints.visits).subscribe((users) => {
      this.visits$.next(users);
    });
    return this.visits$.asObservable();
  }

  public getVisit(visitId: string): Observable<Visit> {
    return this.http.get<Visit>(AppConfig.endpoints.visits + '/' + visitId);
  }

  public createVisit(visit: Visit): Observable<Visit> {
    return this.http.post<Visit>(AppConfig.endpoints.visits, visit, httpOptions)
      .pipe(
        tap((visitCreated) => {
          this.getVisitsAfterThat();
          LoggerService.log(`visit created is=${JSON.stringify(visitCreated)}`);
          this.utils.showSnackBar('VISIT_CREATED');
        }),
        catchError(this.handleError<Visit>('create visit'))
      );
  }

  public updateVisit(visit: Visit): Observable<Visit> {
    return this.http.put<Visit>(AppConfig.endpoints.visits + '/' + visit.id, visit, httpOptions)
      .pipe(
        tap((visitUpdated) => {
          this.getVisitsAfterThat();
          LoggerService.log(`visit updated is=${JSON.stringify(visitUpdated)}`);
          this.utils.showSnackBar('VISIT_UPDATED');
        })
      );
  }

  public removeVisit(visitId: string): any {
    return this.http.delete<any>(AppConfig.endpoints.visits + '/' + visitId)
      .pipe(
        tap(() => {
          this.getVisitsAfterThat();
          LoggerService.log(`visit removed`);
          this.utils.showSnackBar('VISIT_REMOVED');
        })
      );
  }

  private getVisitsAfterThat() {
    this.getVisits();
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      LoggerService.log(`${operation} failed: ${error.message}`);

      switch (error.status) {
        case 500:
          this.utils.showSnackBar('ERROR_500');
          break;
        default:
          break;
      }


      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


}
