import { CategoriesService } from './../../shared/services/categories/categories-service.service';
import { FarmsService } from './../farms/shared/farms.service';
import { AuthenticationService } from './../../shared/services/authentication/authentication.service';
import { VisitsService } from './shared/visits.service';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { MaterialModule } from './../../material.module';
import { VisitsPageComponent } from './pages/visits/visits.page';
import { VisitsRoutingModule } from './visits-routing.module';
import { VisitsTableComponent } from './components/visits-table/visits-table.component';
import { VisitFormComponent } from './components/visit-form/visit-form.component';
import { VisitFormPageComponent } from './pages/visit-form/visit-form.page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TechnicalsService } from '../../shared/services/techincals/techicals.service';
import { VisitSummaryFormPageComponent } from './pages/visits-summary/visits-summary.page';
import { VisitSummaryFormComponent } from './components/visits-summary/visits-summary.component';
import { DatePipe } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    MaterialModule,
    VisitsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    FlexLayoutModule
  ],
  exports: [],
  declarations: [
    VisitsPageComponent,
    VisitsTableComponent,
    VisitFormComponent,
    VisitFormPageComponent,
    VisitSummaryFormPageComponent,
    VisitSummaryFormComponent
  ],
  providers: [
    VisitsService,
    AuthenticationService,
    TechnicalsService,
    FarmsService,
    DatePipe,
    CategoriesService
  ],
})
export class VisitsModule { }
