import { UtilService } from './../../../../shared/services/util/util.service';
import { FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  public resetPasswordForm: FormGroup;
  private passwordValidator: ValidatorFn;

  @Output()
  public submitForm: EventEmitter<{ email: string, password: string }>;

  constructor(
    private formBuilder: FormBuilder,
    private utils: UtilService,
  ) {
    this.submitForm = new EventEmitter();
    this.passwordValidator = Validators.compose([Validators.minLength(8), Validators.required]);
  }

  ngOnInit() {
    this.initForm();
  }

  public submit() {
    if (this.resetPasswordForm.valid) {
      this.submitForm.emit(this.resetPasswordForm.value);
    }
  }

  private initForm() {
    const extra = {
      validator: this.utils.MatchPassword
    };
    this.resetPasswordForm = this.formBuilder.group({
      password: ['', this.passwordValidator],
      confirmPassword: ['', this.passwordValidator]
    }, extra);
  }

}
