import { AuthenticationService } from './../../../../shared/services/authentication/authentication.service';
import { LoggerService } from './../../../../shared/services/logger/logger.service';
import { FormBuilder, FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  public loginForm: FormGroup;
  private emailValidator: ValidatorFn;

  @Output()
  public submitForm: EventEmitter<{ email: string, password: string }>;

  @Output()
  public recoverPassword: EventEmitter<{ email: string }>;

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService
  ) {
    this.submitForm = new EventEmitter();
    this.recoverPassword = new EventEmitter();
  }

  ngOnInit() {
    this.initValidators();
    this.initForm();
  }

  public submit() {
    if (this.loginForm.valid) {
      const data = {
        email: this.loginForm.value.email,
        password: this.loginForm.value.password
      };
      LoggerService.log(JSON.stringify(data));
      this.submitForm.next(data);
    }
  }

  public recoverClicked() {
    if (this.loginForm.get('email').valid) {
      const data = {
        email: this.loginForm.value.email,
      };
      LoggerService.log(JSON.stringify(data));
      this.recoverPassword.next(data);
    }
  }

  private initValidators() {
    this.emailValidator = Validators.compose([Validators.required, Validators.email]);
  }

  private initForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', this.emailValidator],
      password: ['', Validators.required]
    });
  }

}
