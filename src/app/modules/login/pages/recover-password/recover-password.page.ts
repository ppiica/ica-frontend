import { AuthenticationService } from './../../../../shared/services/authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UtilService } from '../../../../shared/services/util/util.service';

@Component({
  selector: 'app-recover-password-page',
  templateUrl: './recover-password.page.html',
  styleUrls: ['./recover-password.page.css']
})
export class RecoverPasswordPageComponent implements OnInit {

  private token: string;

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private activatedRoute: ActivatedRoute,
    private utils: UtilService
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      this.token = params.token;
      this.checkToken(this.token);
    });
  }

  public onSubmitForm({ password }) {
    this.authService.resetPassword(this.token, password)
      .then(data => {
        this.utils.showSnackBar('PASSWORD_HAS_CHANGED');
        this.router.navigate(['login', 'login']);
      })
      .catch(error => {
        this.utils.showSnackBar('ERROR_CHANGING_PASSWORD');
        this.router.navigate(['login', 'login']);
      });
  }

  /**
   * checkToken
   */
  public checkToken(token: string) {
    this.authService.checkTokenToRecoverPassword(token)
      .catch(() => {
        this.utils.showSnackBar('INVALID_TOKEN_TO_RECOVER_PASSWORD');
        this.router.navigate(['login', 'login']);
      });
  }

}
