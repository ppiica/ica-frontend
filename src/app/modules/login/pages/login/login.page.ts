import { AuthenticationService } from './../../../../shared/services/authentication/authentication.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UtilService } from '../../../../shared/services/util/util.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPageComponent implements OnInit {

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private utils: UtilService
  ) { }

  ngOnInit() {
  }

  public onSubmitForm({email, password}) {
    this.authenticationService.login(email, password)
      .then(data => {
        console.log(data);
        this.router.navigate(['admin', 'dashboard']);
      })
      .catch(error => {
        this.utils.showSnackBar('INVALID_PASSWORD');
      });
  }

  public onRecoverPassword({ email }) {
    this.authenticationService.recoverPassword(email)
    .then(data => {
      this.utils.showSnackBar('EMAIL_SEND_TO_RESET_PASSWORD');
    })
    .catch(error => {
      this.utils.showSnackBar('EMAIL_NOT_FOUND');
    });
  }

}
