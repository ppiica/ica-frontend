import { AppConfig } from './../../config/app.config';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginPageComponent } from './pages/login/login.page';
import { RecoverPasswordPageComponent } from './pages/recover-password/recover-password.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
    data: {
      formName: AppConfig.permissions.ANY,
      action: AppConfig.actions.VIEW
    }
  },
  {
    path: 'login',
    component: LoginPageComponent,
    data: {
      formName: AppConfig.permissions.ANY,
      action: AppConfig.actions.VIEW
    }
  },
  {
    path: 'recoverPassword/:token',
    component: RecoverPasswordPageComponent,
    data: {
      formName: AppConfig.permissions.ANY,
      action: AppConfig.actions.VIEW
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule {}
