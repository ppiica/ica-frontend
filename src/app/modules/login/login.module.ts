import { UtilService } from './../../shared/services/util/util.service';
import { AuthenticationService } from './../../shared/services/authentication/authentication.service';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FlexModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoginPageComponent } from './pages/login/login.page';
import { LoginRoutingModule } from './login-routing.module';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { MaterialModule } from './../../material.module';
import { RecoverPasswordPageComponent } from './pages/recover-password/recover-password.page';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    MaterialModule,
    TranslateModule.forChild(),
    FlexModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    LoginPageComponent,
    LoginFormComponent,
    RecoverPasswordPageComponent,
    ResetPasswordComponent
  ],
  providers: [
    AuthenticationService,
    UtilService
  ]
})
export class LoginModule { }
