import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppConfig } from './../../../../config/app.config';
import { DefaultPermissions, Permission } from './../../../../shared/models/user';
import { Subscription } from 'rxjs/Subscription';
import { AuthenticationService } from './../../../../shared/services/authentication/authentication.service';
import { MatDialog } from '@angular/material';
import { DeleteDialogComponent } from '../../../../shared/components/delete-dialog/delete-dialog.component';
import { Router, NavigationExtras } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { UsersService } from './../../users.service';
import { User } from '../../../../shared/models/user';
import { CoordinatorService } from '../../../../shared/services/coordinators/coordinators.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.css']
})
export class UsersPageComponent implements OnInit, OnDestroy {

  private userSubscription: Subscription = null;
  public users: Observable<User[]>;
  public userPermission$: BehaviorSubject<Permission>;


  constructor(
    private usersService: UsersService,
    private matDialog: MatDialog,
    private router: Router,
    private authenticationService: AuthenticationService,
    private coordinatorService: CoordinatorService,
  ) {
    this.userPermission$ = new BehaviorSubject(DefaultPermissions);
  }

  ngOnInit() {
    this.users = this.usersService.getUsers();
    this.validateActions();
  }

  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }


  private onUserChange(user: User) {
    this.userPermission$.next(this.authenticationService.validatePermission(AppConfig.permissions.USERS));
  }

  private async validateActions() {
    if (!this.userSubscription) {
      this.userSubscription = (await this.authenticationService.getLoggedUser()).subscribe(user => this.onUserChange(user));
    }
  }

  /**
  * editUser
  */
  public editUser(user: User) {
    this.usersService.userToEdit = user;
    this.router.navigate(['admin', 'users', 'form', user.id]);
  }
  
  public clickAssignmentTechnical(user: User){
    this.coordinatorService.coordinatorToAssign = user;
    this.router.navigate(['admin', 'users', 'assignment', user.id]);
  }

  public removeUser(userId: string) {
    const dialog = this.matDialog.open(DeleteDialogComponent, {
      data: {
        removeItem: 'REMOVE_USER'
      }
    });
    dialog.beforeClose().subscribe((result) => {
      if (result) {
        this.usersService.removeUser(userId).subscribe();
      }
    });
  }

}
