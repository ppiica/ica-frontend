import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppConfig } from './../../../../config/app.config';
import { Subscription } from 'rxjs/Subscription';
import { AuthenticationService } from './../../../../shared/services/authentication/authentication.service';
import { MatDialog } from '@angular/material';
import { DeleteDialogComponent } from '../../../../shared/components/delete-dialog/delete-dialog.component';
import { Router, NavigationExtras } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Technical } from '../../../../shared/models/technical';
import { Permission, DefaultPermissions, User } from '../../../../shared/models/user';
import { TechnicalsService } from '../../../../shared/services/techincals/techicals.service';
import { UsersService } from '../../users.service';
import { CoordinatorService } from '../../../../shared/services/coordinators/coordinators.service';
import { UtilService } from '../../../../shared/services/util/util.service';


@Component({
  selector: 'assignment-technicians',
  templateUrl: './assignment-technicians.page.html',
  styleUrls: ['./assignment-technicians.page.css']
})
export class AssignmentTechnicianPageComponent implements OnInit, OnDestroy {

  private technicalSubscription: Subscription = null;
  public technicals: Observable<Technical[]>;
  public assignedTechnicals: Observable<Technical[]>;
  public technicalsPermission$: BehaviorSubject<Permission>;
  public coordinator: User;


  constructor(
    private technicalsService: TechnicalsService,
    private matDialog: MatDialog,
    private router: Router,
    private authenticationService: AuthenticationService,
    private coordinatorService: CoordinatorService,
    private utils: UtilService
  ) {
    this.technicalsPermission$ = new BehaviorSubject(DefaultPermissions);
  }

  ngOnInit() {
    this.coordinator = this.coordinatorService.coordinatorToAssign;
    this.technicals = this.technicalsService.getTechnicals();
    if (this.coordinator)
      this.assignedTechnicals = this.coordinatorService.getTechnicalsByCoordinator(this.coordinator.id);
  }

  ngOnDestroy() {
    if (this.technicalSubscription) {
      this.technicalSubscription.unsubscribe();
    }
  }

  private onUserChange(user: User) {
    this.technicalsPermission$.next(this.authenticationService.validatePermission(AppConfig.permissions.USERS));
  }

  private async validateActions() {
    if (!this.technicalSubscription) {
      this.technicalSubscription = (await this.authenticationService.getLoggedUser()).subscribe(user => this.onUserChange(user));
    }
  }

  public clickSaveSelection(data: any[]) {
    if (this.coordinator) {
      this.coordinator.techniciansIds = data.map(selection => selection.id);
      this.coordinatorService.assignTechnicals(this.coordinator.id, this.coordinator)
        .then(data => {
          this.utils.showSnackBar('ASSIGNMENT_TECHNICALS_UPDATE');
          this.router.navigate(['admin', 'users']);
        })
        .catch(error => {
          this.utils.showSnackBar('ASSIGNMENT_TECHNICALS_ERROR');
        });
    }
  }
}
