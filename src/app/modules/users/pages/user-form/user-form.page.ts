import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import { AppConfig } from './../../../../config/app.config';
import { UserType } from './../../../../shared/models/user-types';
import { AuthenticationService } from './../../../../shared/services/authentication/authentication.service';
import { UserTypesService } from './../../../../shared/services/user-types/user-types.service';
import { Router, ActivatedRoute } from '@angular/router';

import { UsersService } from './../../users.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { User, Permission, DefaultPermissions } from '../../../../shared/models/user';
import { Office } from '../../../../shared/models/office';
import { Observable } from 'rxjs/Observable';
import { OfficesService } from '../../../offices/shared/offices.service';

@Component({
  selector: 'app-user-form-page',
  templateUrl: './user-form.page.html',
  styleUrls: ['./user-form.page.css']
})
export class UserFormPageComponent implements OnInit, OnDestroy {

  public mode = 'edit';
  public userData: User;
  public userTypes: UserType[];
  public userPermission$: BehaviorSubject<Permission>;
  public userSubscription: Subscription;
  public offices: Observable<Office[]>;

  constructor(
    private usersService: UsersService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private userTypesService: UserTypesService,
    private authenticationService: AuthenticationService,
    private officesService: OfficesService,
  ) {
    this.userPermission$ = new BehaviorSubject(DefaultPermissions);
  }

  ngOnInit() {
    this.getUser();
    this.getUserTypes();
    this.validateActions();
    this.getOffices();
  }

  ngOnDestroy() {
    delete this.usersService.userToEdit;
  }

  public createUser(user: User) {
    this.usersService.createUser(user).subscribe((res) => {
      this.router.navigate(['admin', 'users', 'list']);
    });
  }

  public updateUser(user: User) {
    this.usersService.updateUser(user).subscribe((res) => {
      this.router.navigate(['admin', 'users', 'list']);
    });
  }

  private getUser() {
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params.mode) {
        this.mode = params.mode;
      }
      if (this.usersService.userToEdit) {
        this.userData = this.usersService.userToEdit;
      } else if (this.mode !== 'create' && params.id ) {
        this.usersService.getUser(params.id).subscribe((user) => {
          this.userData = user;
        });
      }
    });
  }

  private async getUserTypes() {
    this.userTypes = await this.userTypesService.getRoles();
  }

  private onUserChange(user: User) {
    this.userPermission$.next(this.authenticationService.validatePermission(AppConfig.permissions.USERS));
  }

  private async validateActions() {
    if (!this.userSubscription) {
      this.userSubscription = (await this.authenticationService.getLoggedUser()).subscribe(user => this.onUserChange(user));
    }
  }

  private getOffices() {
    this.offices = this.officesService.getOffices();
  }

}
