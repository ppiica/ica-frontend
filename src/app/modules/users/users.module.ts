import { AuthenticationService } from './../../shared/services/authentication/authentication.service';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { MaterialModule } from './../../material.module';
import { UsersRoutingModule } from './users-routing.module';
import { UsersPageComponent } from './pages/users/users.page';
import { UsersTableComponent } from './components/users-table/users-table.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { UsersService } from './users.service';
import { UserFormComponent } from './components/user-form/user-form.component';
import { UserFormPageComponent } from './pages/user-form/user-form.page';
import { SharedModule } from '../../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { UserTypesService } from '../../shared/services/user-types/user-types.service';
import { AssignmentTechnicianPageComponent } from './pages/assignment-technicians/assignment-technicians.page';
import { AssignmentTechnicianTableComponent } from './components/assignment-technician/assignment-technician.component';
import { TechnicalsService } from '../../shared/services/techincals/techicals.service';
import { CoordinatorService } from '../../shared/services/coordinators/coordinators.service';

@NgModule({
  declarations: [
    UsersPageComponent,
    UsersTableComponent,
    UserDetailComponent,
    UserFormComponent,
    UserFormPageComponent,
    AssignmentTechnicianTableComponent,
    AssignmentTechnicianPageComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    TranslateModule.forChild(),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    FlexLayoutModule
  ],
  exports: [],
  providers: [
    UsersService,
    AuthenticationService,
    UserTypesService,
    TechnicalsService,
    CoordinatorService
  ],
})
export class UsersModule { }
