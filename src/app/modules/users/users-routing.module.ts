import { AppConfig } from './../../config/app.config';
import { AuthGuardService } from './../../shared/services/authentication/auth-guard.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersPageComponent } from './pages/users/users.page';
import { UserFormPageComponent } from './pages/user-form/user-form.page';
import { AssignmentTechnicianPageComponent } from './pages/assignment-technicians/assignment-technicians.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
    data: {formName: AppConfig.permissions.USERS , action: AppConfig.actions.VIEW }
  },
  {
    path: 'list',
    component: UsersPageComponent,
    canActivate: [AuthGuardService],
    data: {formName: AppConfig.permissions.USERS , action: AppConfig.actions.VIEW }
  },
  {
    path: 'form/:id',
    component: UserFormPageComponent,
    canActivate: [AuthGuardService],
    data: {formName: AppConfig.permissions.USERS , action: AppConfig.actions.CREATE }
  },
  {
    path: 'assignment/:id',
    component: AssignmentTechnicianPageComponent,
    canActivate: [AuthGuardService],
    data: {formName: AppConfig.permissions.USERS , action: AppConfig.actions.VIEW }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule { }

export const routedComponents = [UsersPageComponent, UserFormPageComponent, AssignmentTechnicianPageComponent];
