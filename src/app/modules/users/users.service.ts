import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';


import { User } from './../../shared/models/user';
import { AppConfig } from '../../config/app.config';
import { LoggerService } from '../../shared/services/logger/logger.service';
import { UtilService } from '../../shared/services/util/util.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class UsersService {

  private users$: BehaviorSubject<User[]>;

  public userToEdit: User;

  constructor(
    private http: HttpClient,
    private utils: UtilService
  ) {
    this.users$ = new BehaviorSubject<User[]>([]);
  }


  public getUsers(): Observable<User[]> {
    this.http.get<User[]>(AppConfig.endpoints.users).subscribe((users) => {
      this.users$.next(users);
    });
    return this.users$.asObservable();
  }

  public getUser(userId: string) {
    return this.http.get<User>(AppConfig.endpoints.users + '/' + userId);
  }

  public createUser(user: User): Observable<User> {
    return this.http.post<User>(AppConfig.endpoints.users, user, httpOptions)
      .pipe(
        tap((userCreated) => {
          this.getUsersAfterThat();
          LoggerService.log(`user created is=${JSON.stringify(userCreated)}`);
          this.utils.showSnackBar('USER_CREATED');
        }),
        catchError(this.handleError<User>('create user'))
      );
  }

  public updateUser(user: User): Observable<User> {
    return this.http.put<User>(AppConfig.endpoints.users + '/' + user.id, user, httpOptions)
      .pipe(
        tap((userUpdated) => {
          this.getUsersAfterThat();
          LoggerService.log(`user updated is=${JSON.stringify(userUpdated)}`);
          this.utils.showSnackBar('USER_UPDATED');
        })
      );
  }

  public removeUser(userId: string): Observable<any> {
    return this.http.delete<any>(AppConfig.endpoints.users + '/' + userId)
      .pipe(
        tap(() => {
          this.getUsersAfterThat();
          LoggerService.log(`user removed`);
          this.utils.showSnackBar('USER_REMOVED');
        })
      );
  }

  private getUsersAfterThat() {
    this.getUsers();
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      LoggerService.log(`${operation} failed: ${error.message}`);

      switch (error.status) {
        case 500:
          this.utils.showSnackBar('ERROR_500');
          break;
        case 409:
          this.utils.showSnackBar('USER_ALREADY_EXISTS');
          break;
        default:
          break;
      }


      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
