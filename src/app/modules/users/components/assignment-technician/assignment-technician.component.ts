import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Permission, User } from './../../../../shared/models/user';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { debounceTime, distinctUntilChanged, startWith, tap, delay } from 'rxjs/operators';
import { merge } from 'rxjs/observable/merge';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { Component, OnInit, ViewChild, AfterViewInit, Input, Output, EventEmitter, OnDestroy, ElementRef } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { SelectionModel } from '@angular/cdk/collections';
import { Technical } from '../../../../shared/models/technical';

@Component({
  selector: 'assignment-technician-table',
  templateUrl: './assignment-technician.component.html',
  styleUrls: ['./assignment-technician.component.css']
})
export class AssignmentTechnicianTableComponent implements OnInit, AfterViewInit, OnDestroy {


  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  @ViewChild('input') input: ElementRef;

  public technicalDataSource: MatTableDataSource<Technical>;
  public selection: SelectionModel<Technical> = new SelectionModel<Technical>(true, []);;
  private technicalsSubscription: Subscription;
  private assignedTechnicalsSubscription: Subscription;
  public technicals: Technical[];
  private technicalsPermissionsSubscription: Subscription;
  public technicalsPermissions: Permission;
  public displayedColumns = ['select', 'id', 'firstName', 'lastNameOne', 'professional_registration'];

  @Input()
  public coordinator: User;

  @Input()
  public technicalsObservable: Observable<Technical[]>;

  @Input()
  public assignedTechnicalsObservable: Observable<Technical[]>;

  @Input()
  public technicalsPermission$: BehaviorSubject<Permission>;

  @Output()
  public clickAssignmentTechnical: EventEmitter<Technical>

  @Output()
  public clickSaveSelection: EventEmitter<any> = new EventEmitter();

  constructor() {
    this.clickAssignmentTechnical = new EventEmitter();
    this.clickSaveSelection = new EventEmitter();
  }

  ngOnInit() {
    this.technicalDataSource = new MatTableDataSource([]);
    this.loadData();
    this.validateActions();
  }

  ngAfterViewInit() {
    this.technicalDataSource.paginator = this.paginator;
    this.technicalDataSource.sort = this.sort;
  }

  ngOnDestroy() {
    if (this.technicalsSubscription) {
      this.technicalsSubscription.unsubscribe();
    }
  }

  public applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.technicalDataSource.filter = filterValue;
  }

  private loadData() {
    this.technicalsSubscription = this.technicalsObservable.subscribe((technicals) => {
      this.technicalDataSource.data = technicals;
      this.technicalDataSource.data.forEach((element) => {
        if(element.coordinatorId === this.coordinator.id){
          this.selection.select(element)
        }
      })

    });
  }

  private validateActions() {
    if (!this.technicalsPermissionsSubscription) {
      this.technicalsPermissionsSubscription = this.technicalsPermission$.subscribe((permissions: Permission) => {
        this.technicalsPermissions = permissions;
      });
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.technicalDataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.technicalDataSource.data.forEach(row => this.selection.select(row));
  }
}
