import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import { UtilService } from './../../../../shared/services/util/util.service';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, FormControl } from '@angular/forms';
import { MAT_CHECKBOX_CLICK_ACTION } from '@angular/material';
import { AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { User, Permission } from '../../../../shared/models/user';
import { UserType } from './../../../../shared/models/user-types';
import { Office } from '../../../../shared/models/office';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit, OnDestroy {

  public userForm: FormGroup;
  public title: string;
  private requiredMaxLengthValidatorId: ValidatorFn;
  private optionalMaxLengthValidator: ValidatorFn;
  private emailValidator: ValidatorFn;
  private passwordValidator: ValidatorFn;
  private onlyTextValidator: ValidatorFn;
  private onlyNumbersValidator: ValidatorFn;
  private userPermissionsSubscription: Subscription;
  public userPermissions: Permission;
  private requiredMaxLengthValidatorName: ValidatorFn;
  private readonly coordinatorFields = [{
    name: 'office',
  }];
  private readonly technicalFields = [{
    name: 'professionalRegistration',
  }];
  public activeCoordinatorsFields = false;
  public activeTechnicalFields = false;

  @Input()
  public mode: string;

  @Input()
  public user: User;

  @Input()
  public userTypes: Observable<UserType[]>;

  @Input()
  public userPermission$: BehaviorSubject<Permission>;

  @Input()
  public offices: Observable<Office[]>;

  @Output()
  public submitCreate: EventEmitter<User>;

  @Output()
  public submitUpdate: EventEmitter<User>;

  constructor(
    private formBuilder: FormBuilder,
    private utils: UtilService,
  ) {
    this.submitCreate = new EventEmitter();
    this.submitUpdate = new EventEmitter();
    this.initValidators();
  }

  ngOnInit() {
    console.log(this.mode);
    this.title = this.mode === 'edit' ? 'EDIT_USER' : 'CREATE_USER';
    this.initForm(this.user);
    this.validateActions();
  }

  ngOnDestroy() {
    if (this.userPermissionsSubscription) {
      this.userPermissionsSubscription.unsubscribe();
    }
  }

  public submit() {
    const user = this.prepareDataToSend();
    if (this.mode === 'create') {
      this.submitCreate.emit(user);
    } else if (this.mode === 'edit') {
      this.submitUpdate.emit(user);
    }
  }

  public onRoleSelected(roleId: number) {
    console.log(roleId);

    this.activeCoordinatorsFields = false;
    this.activeTechnicalFields = false;
    this.removeControls(this.coordinatorFields);
    this.removeControls(this.technicalFields);
    switch (roleId) {
      case 3:
        this.setNewControls(this.coordinatorFields);
        this.activeCoordinatorsFields = true;
        break;
      case 4:
        this.setNewControls(this.technicalFields);
        this.activeTechnicalFields = true;
        break;
    }
  }

  private setNewControls(controls: any[]) {
    if (controls) {
      for (let i = 0; i < controls.length; i++) {
        const field = controls[i];
        this.userForm.addControl(field.name, new FormControl());
      }
    }
  }

  private removeControls(controls: any[]) {
    console.log(controls);
    if (controls) {
      for (let i = 0; i < controls.length; i++) {
        const field = controls[i];
        this.userForm.removeControl(field.name);
      }
    }
  }

  private initValidators() {
    this.requiredMaxLengthValidatorId = Validators.compose([Validators.minLength(2), Validators.maxLength(11), Validators.required]);
    this.optionalMaxLengthValidator = Validators.compose([Validators.minLength(2), Validators.maxLength(45)]);
    this.emailValidator = Validators.compose([Validators.email, Validators.required, Validators.minLength(2), Validators.maxLength(254)]);
    this.passwordValidator = Validators.compose([Validators.minLength(8), Validators.maxLength(30), Validators.required]);
    this.onlyTextValidator = Validators.pattern(new RegExp('^[a-zA-z áéíóúÁÉÍÓÚñÑ]*$'));
    this.onlyNumbersValidator = Validators.pattern(new RegExp('^[1-9][0-9]*$'));
    this.requiredMaxLengthValidatorName = Validators.compose([Validators.minLength(2), Validators.maxLength(45), Validators.required]);
  }

  private initForm(user?: User) {

    const groupConfig = {
      id: ['', [this.requiredMaxLengthValidatorId, this.onlyNumbersValidator]],
      firstName: ['', [this.requiredMaxLengthValidatorName, this.onlyTextValidator]],
      lastNameOne: ['', [this.requiredMaxLengthValidatorName, this.onlyTextValidator]],
      lastNameTwo: ['', [this.optionalMaxLengthValidator, this.onlyTextValidator]],
      email: ['', this.emailValidator],
      role: [undefined, Validators.required],
      password: ['', this.passwordValidator],
      confirmPassword: ['', this.passwordValidator],
    };

    const extra = {
      validator: this.utils.MatchPassword
    };

    if (this.mode === 'edit') {
      delete groupConfig.password;
      delete groupConfig.confirmPassword;
      delete extra.validator;
    }

    this.userForm = this.formBuilder.group(groupConfig, extra);
    if (user) {
      this.onRoleSelected(user.roleId);
      const userSet: any = {
        id: user.id,
        firstName: (user.profile) ? user.profile.firstName : '',
        lastNameOne: (user.profile) ? user.profile.lastNameOne : '',
        lastNameTwo: (user.profile) ? user.profile.lastNameTwo : '',
        email: (user.email) ? user.email : '',
        role: user.roleId,
      };

      if (user.profile && user.profile.professionalRegistration) {
        userSet.professionalRegistration = user.profile.professionalRegistration;
      }

      if (user.profile && user.profile.officeId) {
        userSet.office = user.profile.officeId;
      }

      this.userForm.setValue(userSet);
    }
  }

  private prepareDataToSend(): User {
    const formValue = this.userForm.value;
    const user: any = {
      id: formValue.id,
      email: formValue.email,
      profile: {
        firstName: formValue.firstName,
        lastNameOne: formValue.lastNameOne,
      },
      roleId: formValue.role,
      password: formValue.password,
    };

    if (formValue.lastNameTwo) {
      user.profile.lastNameTwo = formValue.lastNameTwo;
    }

    if (formValue.professionalRegistration) {
      user.profile.professionalRegistration = formValue.professionalRegistration;
    }

    if (formValue.office) {
      user.profile.officeId = formValue.office;
    }

    return user;
  }

  private validateActions() {
    if (!this.userPermissionsSubscription) {
      this.userPermissionsSubscription = this.userPermission$.subscribe((permission: Permission) => {
        this.userPermissions = permission;
      });
    }
  }

}
