import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Permission } from './../../../../shared/models/user';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { debounceTime, distinctUntilChanged, startWith, tap, delay } from 'rxjs/operators';
import { merge } from 'rxjs/observable/merge';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { Component, OnInit, ViewChild, AfterViewInit, Input, Output, EventEmitter, OnDestroy, ElementRef } from '@angular/core';

import { User } from '../../../../shared/models/user';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.css']
})
export class UsersTableComponent implements OnInit, AfterViewInit, OnDestroy {


  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  @ViewChild('input') input: ElementRef;

  public userDataSource: MatTableDataSource<User>;

  private usersSubscription: Subscription;

  private usersPermissionsSubscription: Subscription;

  public usersPermissions: Permission;

  public displayedColumns = ['id', 'firstName', 'email', 'userType', 'actions'];

  @Input()
  public usersObservable: Observable<User[]>;

  @Input()
  public userPermission$: BehaviorSubject<Permission>;

  @Output()
  public clickEditUser: EventEmitter<User>;

  @Output()
  public clickRemoveUser: EventEmitter<User>;

  @Output()
  public clickAssignmentTechnical: EventEmitter<User>;

  constructor() {
    this.clickEditUser = new EventEmitter();
    this.clickRemoveUser = new EventEmitter();
    this.clickAssignmentTechnical = new EventEmitter();
  }

  ngOnInit() {
    this.userDataSource = new MatTableDataSource([]);
    this.loadData();
    this.validateActions();
  }

  ngAfterViewInit() {
    this.userDataSource.paginator = this.paginator;
    this.userDataSource.sort = this.sort;
  }

  ngOnDestroy() {
    if (this.usersSubscription) {
      this.usersSubscription.unsubscribe();
    }
  }

  public applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.userDataSource.filter = filterValue;
  }

  private loadData() {
    this.usersSubscription = this.usersObservable.subscribe((users) => {
      this.userDataSource.data = users;
    });
  }

  private validateActions() {
    if (!this.usersPermissionsSubscription) {
      this.usersPermissionsSubscription = this.userPermission$.subscribe((permissions: Permission) => {
        this.usersPermissions = permissions;
      });
    }
  }

}
