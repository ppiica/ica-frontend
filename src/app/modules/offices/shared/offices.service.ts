import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

import { LoggerService } from '../../../shared/services/logger/logger.service';
import { UtilService } from '../../../shared/services/util/util.service';
import { AppConfig } from '../../../config/app.config';
import { Office } from '../../../shared/models/office';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class OfficesService {

  public offices$: BehaviorSubject<Office[]>;
  public officeToEdit: Office;

  constructor(
    private http: HttpClient,
    private utils: UtilService
  ) {
    this.offices$ = new BehaviorSubject<Office[]>([]);
  }

  public getOffices(): Observable<Office[]> {
    this.http.get<Office[]>(AppConfig.endpoints.offices).subscribe((offices) => {
      this.offices$.next(offices);
    });
    return this.offices$.asObservable();
  }

  public getOffice(officeId: string): Observable<Office> {
    return this.http.get<Office>(AppConfig.endpoints.offices + '/' + officeId);
  }

  public createOffice(office: Office): Observable<Office> {
    return this.http.post<Office>(AppConfig.endpoints.offices, office, httpOptions)
      .pipe(
      tap((officeCreated) => {
        this.getOfficesAfterThat();
        LoggerService.log(`office created is=${JSON.stringify(officeCreated)}`);
        this.utils.showSnackBar('OFFICE_CREATED');
      }),
      catchError(this.handleError<Office>('create office'))
      );
  }

  public updateOffice(office: Office): Observable<Office> {
    return this.http.put<Office>(AppConfig.endpoints.offices + '/' + office.id, office, httpOptions)
      .pipe(
      tap((officeUpdated) => {
        this.getOfficesAfterThat();
        LoggerService.log(`office updated is=${JSON.stringify(officeUpdated)}`);
        this.utils.showSnackBar('OFFICE_UPDATED');
      })
      );
  }

  public removeOffice(officeId: string): any {
    return this.http.delete<any>(AppConfig.endpoints.offices + '/' + officeId)
      .pipe(
      tap(() => {
        this.getOfficesAfterThat();
        LoggerService.log(`office removed`);
        this.utils.showSnackBar('OFFICE_REMOVED');
      })
      );
  }

  private getOfficesAfterThat() {
    this.getOffices();
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for offices consumption
      LoggerService.log(`${operation} failed: ${error.message}`);

      switch (error.status) {
        case 500:
          this.utils.showSnackBar('ERROR_500');
          break;
        case 410:
          this.utils.showSnackBar('OFFICE_ALREADY_EXITS');
          break;
        case 404:
          this.utils.showSnackBar('OFFICES_NOT_FOUND');
          break;
        default:
          break;
      }


      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


}
