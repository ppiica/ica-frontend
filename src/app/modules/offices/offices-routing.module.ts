import { AppConfig } from './../../config/app.config';
import { AuthGuardService } from './../../shared/services/authentication/auth-guard.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OfficesPageComponent } from './pages/offices/offices.page';
import { OfficeFormPageComponent } from './pages/office-form/office-form.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
    data: {formName: AppConfig.permissions.CRUDS , action: AppConfig.actions.VIEW }
  }, {
    path: 'list',
    component: OfficesPageComponent,
    canActivate: [AuthGuardService],
    data: {formName: AppConfig.permissions.CRUDS , action: AppConfig.actions.VIEW }
  }, {
    path: 'form/:officeId',
    component: OfficeFormPageComponent,
    canActivate: [AuthGuardService],
    data: {formName: AppConfig.permissions.CRUDS , action: AppConfig.actions.SHOW_FORM }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OfficesRoutingModule { }

export const routedComponents = [OfficesPageComponent];
