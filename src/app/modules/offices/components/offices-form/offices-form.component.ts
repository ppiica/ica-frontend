import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import { Permission, DefaultPermissions } from './../../../../shared/models/user';
import { FormBuilder, FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { Component, OnInit, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Office } from '../../../../shared/models/office';

@Component({
    selector: 'app-office-form',
    templateUrl: './offices-form.component.html',
    styleUrls: ['./offices-form.component.css']
})
export class OfficeFormComponent implements OnInit, OnDestroy {

    public officeForm: FormGroup;
    public title: string;
    public officesPermissions = DefaultPermissions;
    private officesPermissionsSubscription: Subscription;
    private validatorFields: ValidatorFn;

    @Input()
    public mode: string;

    @Input()
    public office: Office;

    @Input()
    public officesPermission$: BehaviorSubject<Permission>;

    @Output()
    public submitCreate: EventEmitter<Office>;

    @Output()
    public submitUpdate: EventEmitter<Office>;

    constructor(
        private formBuilder: FormBuilder
    ) {
        this.submitCreate = new EventEmitter();
        this.submitUpdate = new EventEmitter();
        this.officesPermissions = DefaultPermissions;
        this.initValidators();
    }

    private initValidators() {
        this.validatorFields = Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(255)]);
      }

    ngOnInit() {
        console.log(this.mode);
        this.title = this.mode === 'edit' ? 'EDIT_OFFICE' : 'CREATE_OFFICE';
        this.loadData();
        this.initForm(this.office);
    }

    ngOnDestroy() {
        if (this.officesPermissionsSubscription) {
            this.officesPermissionsSubscription.unsubscribe();
        }
    }

    private initForm(office?: Office) {
        this.officeForm = this.formBuilder.group({
            id: [undefined, this.validatorFields],
            name: [undefined, this.validatorFields]
        });

        if (office) {
            const officeSet = {
                id: office.id,
                name: office.name
            };
            this.officeForm.setValue(officeSet);
        }
    }

    private prepareDataToSend(): Office {
        const formValue = this.officeForm.value;
        const office = {
            id: formValue.id,
            name: formValue.name,
        };
        return office;
    }

    public submit() {
        const office = this.prepareDataToSend();
        if (this.mode === 'create') {
            this.submitCreate.emit(office);
        } else if (this.mode === 'edit') {
            this.submitUpdate.emit(office);
        }
    }

    private loadData() {
        this.officesPermissionsSubscription = this.officesPermission$.subscribe((permissions: Permission) => {
            this.officesPermissions = permissions;
        });
    }
}
