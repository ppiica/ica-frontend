import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Permission, DefaultPermissions } from './../../../../shared/models/user';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';
import { } from 'events';
import { Office } from '../../../../shared/models/office';

@Component({
  selector: 'app-offices-table',
  templateUrl: './offices-table.component.html',
  styleUrls: ['./offices-table.component.css']
})
export class OfficesTableComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  @ViewChild('input') input: ElementRef;

  public officesDataSource: MatTableDataSource<Office>;

  private officesSubscription: Subscription;

  public officesPermissions: Permission;

  private officesPermissionsSubscription: Subscription;

  public displayedColumns = ['id', 'name', 'actions'];

  @Input()
  public officesObservable: Observable<Office[]>;

  @Input()
  public officesPermission$: BehaviorSubject<Permission>;

  @Output()
  public clickEditOffice: EventEmitter<Office>;

  @Output()
  public clickRemoveOffice: EventEmitter<Office>;

  constructor() {
    this.clickEditOffice = new EventEmitter();
    this.clickRemoveOffice = new EventEmitter();
    this.officesPermissions = DefaultPermissions;
  }

  ngOnInit() {
    this.officesDataSource = new MatTableDataSource([]);
    this.loadData();
  }

  ngAfterViewInit() {
    this.officesDataSource.paginator = this.paginator;
    this.officesDataSource.sort = this.sort;
  }

  ngOnDestroy() {
    if (this.officesSubscription) {
      this.officesSubscription.unsubscribe();
    }
    if (this.officesPermissionsSubscription) {
      this.officesPermissionsSubscription.unsubscribe();
    }
  }

  public applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.officesDataSource.filter = filterValue;
  }

  private loadData() {
    this.officesSubscription = this.officesObservable.subscribe((offices) => {
      this.officesDataSource.data = offices;
    });
    this.officesPermissionsSubscription = this.officesPermission$.subscribe((permission: Permission) => {
      this.officesPermissions = permission;
    });
  }
}
