import { AuthenticationService } from './../../shared/services/authentication/authentication.service';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from './../../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { OfficeFormPageComponent } from './pages/office-form/office-form.page';
import { OfficeFormComponent } from './components/offices-form/offices-form.component';
import { OfficesPageComponent } from './pages/offices/offices.page';
import { OfficesTableComponent } from './components/offices-table/offices-table.component';
import { OfficesRoutingModule } from './offices-routing.module';
import { OfficesService } from './shared/offices.service';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    MaterialModule,
    OfficesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    FlexLayoutModule
  ],
  exports: [],
  declarations: [
    OfficesPageComponent,
    OfficesTableComponent,
    OfficeFormComponent,
    OfficeFormPageComponent
  ],
  providers: [
    OfficesService,
    AuthenticationService
  ],
})
export class OfficesModule { }
