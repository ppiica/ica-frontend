import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppConfig } from './../../../../config/app.config';
import { User, DefaultPermissions, Permission } from './../../../../shared/models/user';
import { LoggerService } from './../../../../shared/services/logger/logger.service';
import { AuthenticationService } from './../../../../shared/services/authentication/authentication.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { DeleteDialogComponent } from '../../../../shared/components/delete-dialog/delete-dialog.component';
import { Office } from '../../../../shared/models/office';
import { OfficesService } from '../../shared/offices.service';

@Component({
  selector: 'app-offices',
  templateUrl: './offices.page.html',
  styleUrls: ['./offices.page.css']
})
export class OfficesPageComponent implements OnInit {

  private userSubscription = null;
  public offices: Observable<Office[]>;
  public officePermission$: BehaviorSubject<Permission>;

  constructor(
    private officesService: OfficesService,
    private router: Router,
    private matDialog: MatDialog,
    private authenticationService: AuthenticationService
  ) {
    this.officePermission$ = new BehaviorSubject(DefaultPermissions);
  }

  ngOnInit() {
    this.offices = this.officesService.getOffices();
    this.validateActions();
  }

  private onUserChange(user: User) {
    this.officePermission$.next(this.authenticationService.validatePermission(AppConfig.permissions.CRUDS));
  }

  private async validateActions() {
    if (!this.userSubscription) {
      this.userSubscription = (await this.authenticationService.getLoggedUser()).subscribe(user => this.onUserChange(user));
    }
  }

  public editOffice(office: Office) {
    this.officesService.officeToEdit = office;
    this.router.navigate(['admin', 'offices', 'form', office.id]);
  }

  public removeOffice(officeId: string) {
    const dialog = this.matDialog.open(DeleteDialogComponent, {
      data: {
        removeItem: 'REMOVE_OFFICE'
      }
    });
    dialog.beforeClose().subscribe((result) => {
      if (result) {
        this.officesService.removeOffice(officeId).subscribe();
      }
    });
  }
}