import { LoggerService } from './../../../../shared/services/logger/logger.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppConfig } from './../../../../config/app.config';
import { Permission, DefaultPermissions, User } from './../../../../shared/models/user';
import { Observable } from 'rxjs/Observable';
import { RegionsService } from './../../../../shared/services/regions/regions.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Region } from '../../../../shared/models/regions';
import { AuthenticationService } from '../../../../shared/services/authentication/authentication.service';
import { Subscription } from 'rxjs/Subscription';
import { Office } from '../../../../shared/models/office';
import { Owner } from '../../../../shared/models/owners';
import { OwnersService } from '../../../owners/owners.service';
import { OfficesService } from '../../shared/offices.service';

@Component({
  selector: 'app-office-page-form',
  templateUrl: './office-form.page.html',
  styleUrls: ['./office-form.page.css']
})
export class OfficeFormPageComponent implements OnInit, OnDestroy {

  public mode = 'edit';
  public officeData: Office;
  public officePermission$: BehaviorSubject<Permission>;
  public userSubscription: Subscription;
  public offices: Observable<Office[]>;

  constructor(
    private officesService: OfficesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private regionsService: RegionsService,
    private authenticationService: AuthenticationService,
    private ownersService: OwnersService
  ) {
    this.officePermission$ = new BehaviorSubject(DefaultPermissions);
  }

  ngOnInit() {
    this.getOffice();
    this.validateActions();
    this.getOffices();

  }

  ngOnDestroy() {
    delete this.officesService.officeToEdit;
  }

  public createOffice(office: Office) {
    this.officesService.createOffice(office).subscribe((res) => {
      this.router.navigate(['admin', 'offices', 'list']);
    });
  }

  public updateOffice(office: Office) {
    this.officesService.updateOffice(office).subscribe((res) => {
      this.router.navigate(['admin', 'offices', 'list']);
    });
  }
  private getOffice() {
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params.mode) {
        this.mode = params.mode;
      }
      if (this.officesService.officeToEdit) {
        this.officeData = this.officesService.officeToEdit;
      } else if (this.mode !== 'create' && params.id) {
        this.officesService.getOffice(params.id).subscribe((office) => {
          this.officeData = office;
        });
      }
    });
  }

  private getOffices() {
    this.offices = this.officesService.getOffices();
  }

  private onUserChange(user: User) {
    const permissions = this.authenticationService.validatePermission(AppConfig.permissions.CRUDS);
    this.officePermission$.next(permissions);
  }

  private async validateActions() {
    if (!this.userSubscription) {
      this.userSubscription = (await this.authenticationService.getLoggedUser()).subscribe(user => this.onUserChange(user));
    }
  }

}
