import { AppConfig } from './../../config/app.config';
import { AuthGuardService } from './../../shared/services/authentication/auth-guard.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FarmFormPageComponent } from './pages/farm-form/farm-form.page';

import { FarmsPageComponent } from './pages/farms/farms.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
    data: {formName: AppConfig.permissions.ANY , action: AppConfig.actions.VIEW }
  }, {
    path: 'list',
    component: FarmsPageComponent,
    canActivate: [AuthGuardService],
    data: {formName: AppConfig.permissions.FARMS , action: AppConfig.actions.VIEW }
  }, {
    path: 'form/:registerNumber',
    component: FarmFormPageComponent,
    canActivate: [AuthGuardService],
    data: {formName: AppConfig.permissions.FARMS , action: AppConfig.actions.SHOW_FORM }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FarmsRoutingModule { }

export const routedComponents = [FarmsPageComponent];
