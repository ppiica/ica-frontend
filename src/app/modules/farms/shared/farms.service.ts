import { Farm } from './../../../shared/models/farms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

import { LoggerService } from '../../../shared/services/logger/logger.service';
import { UtilService } from '../../../shared/services/util/util.service';
import { AppConfig } from '../../../config/app.config';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class FarmsService {

  public farms$: BehaviorSubject<Farm[]>;
  public farmToEdit: Farm;

  constructor(
    private http: HttpClient,
    private utils: UtilService
  ) {
    this.farms$ = new BehaviorSubject<Farm[]>([]);
  }

  public getFarms(): Observable<Farm[]> {
    this.http.get<Farm[]>(AppConfig.endpoints.farms).subscribe((farms) => {
      this.farms$.next(farms);
    });
    return this.farms$.asObservable();
  }

  public getFarm(farmRegisterNumber: string): Observable<Farm> {
    return this.http.get<Farm>(AppConfig.endpoints.farms + '/' + farmRegisterNumber);
  }

  public createFarm(farm: Farm): Observable<Farm> {
    return this.http.post<Farm>(AppConfig.endpoints.farms, farm, httpOptions)
      .pipe(
      tap((farmCreated) => {
        this.getFarmsAfterThat();
        LoggerService.log(`farm created is=${JSON.stringify(farmCreated)}`);
        this.utils.showSnackBar('FARM_CREATED');
      }),
      catchError(this.handleError<Farm>('create farm'))
      );
  }

  public updateFarm(farm: Farm): Observable<Farm> {
    return this.http.put<Farm>(AppConfig.endpoints.farms + '/' + farm.registerNumber, farm, httpOptions)
      .pipe(
      tap((farmUpdated) => {
        this.getFarmsAfterThat();
        LoggerService.log(`farm updated is=${JSON.stringify(farmUpdated)}`);
        this.utils.showSnackBar('FARM_UPDATED');
      })
      );
  }

  public removeFarm(farmRegisterNumber: string): any {
    return this.http.delete<any>(AppConfig.endpoints.farms + '/' + farmRegisterNumber)
      .pipe(
      tap(() => {
        this.getFarmsAfterThat();
        LoggerService.log(`farm removed`);
        this.utils.showSnackBar('FARM_REMOVED');
      })
      );
  }

  private getFarmsAfterThat() {
    this.getFarms();
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for farms consumption
      LoggerService.log(`${operation} failed: ${error.message}`);

      switch (error.status) {
        case 500:
          this.utils.showSnackBar('ERROR_500');
          break;
        case 410:
          this.utils.showSnackBar('FARM_ALREADY_EXITS');
          break;
        case 404:
          this.utils.showSnackBar('FARMS_NOT_FOUND');
          break;
        default:
          break;
      }


      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


}
