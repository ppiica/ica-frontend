import { LoggerService } from './../../../../shared/services/logger/logger.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppConfig } from './../../../../config/app.config';
import { Permission, DefaultPermissions, User } from './../../../../shared/models/user';
import { Observable } from 'rxjs/Observable';
import { RegionsService } from './../../../../shared/services/regions/regions.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Farm } from '../../../../shared/models/farms';
import { FarmsService } from '../../shared/farms.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Region } from '../../../../shared/models/regions';
import { AuthenticationService } from '../../../../shared/services/authentication/authentication.service';
import { Subscription } from 'rxjs/Subscription';
import { Office } from '../../../../shared/models/office';
import { Owner } from '../../../../shared/models/owners';
import { OwnersService } from '../../../owners/owners.service';
import { OfficesService } from '../../../offices/shared/offices.service';

@Component({
  selector: 'app-farm-page-form',
  templateUrl: './farm-form.page.html',
  styleUrls: ['./farm-form.page.css']
})
export class FarmFormPageComponent implements OnInit, OnDestroy {

  public mode = 'edit';
  public farmData: Farm;
  public departments: Observable<Region[]>;
  public municipalities: Observable<Region[]>;
  public sidewalks: Observable<Region[]>;
  public farmPermission$: BehaviorSubject<Permission>;
  public userSubscription: Subscription;
  public offices: Observable<Office[]>;
  public owners: Observable<Owner[]>;
  public farms: Observable<Farm[]>;

  constructor(
    private farmsService: FarmsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private regionsService: RegionsService,
    private authenticationService: AuthenticationService,
    private officesService: OfficesService,
    private ownersService: OwnersService
  ) {
    this.farmPermission$ = new BehaviorSubject(DefaultPermissions);
  }

  ngOnInit() {
    this.getFarm();
    this.getDepartments();
    this.validateActions();
    this.getOffices();
    this.getOwners();
    this.getFarms();
    if(this.farmData){
      this.getMunicipalities(this.farmData.sidewalk.municipality.department.id);
      this.getSideWalks(this.farmData.sidewalk.municipality.id);
    }
  }

  ngOnDestroy() {
    delete this.farmsService.farmToEdit;
  }

  public createFarm(farm: Farm) {
    this.farmsService.createFarm(farm).subscribe((res) => {
      this.router.navigate(['admin', 'farms', 'list']);
    });
  }

  public updateFarm(farm: Farm) {
    this.farmsService.updateFarm(farm).subscribe((res) => {
      this.router.navigate(['admin', 'farms', 'list']);
    });
  }

  public onSelectDepartment(departmentId: number) {
    this.municipalities = this.regionsService.getMunicipalities(departmentId);
  }

  public onSelectMunicipality(municipalityId: number) {
    this.sidewalks = this.regionsService.getSidewalks(municipalityId);
  }

  private getFarm() {
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params.mode) {
        this.mode = params.mode;
      }
      if (this.farmsService.farmToEdit) {
        this.farmData = this.farmsService.farmToEdit;
      } else if (this.mode !== 'create' && params.id) {
        this.farmsService.getFarm(params.id).subscribe((farm) => {
          this.farmData = farm;
        });
      }
    });
  }

  private getDepartments() {
    this.departments = this.regionsService.getDepartments();
  }

  private getMunicipalities(departmentId: number){
    this.municipalities = this.regionsService.getMunicipalities(departmentId);
  }

  private getSideWalks(municipalityId: number){
    this.sidewalks = this.regionsService.getSidewalks(municipalityId);
  }

  private getOffices() {
    this.offices = this.officesService.getOffices();
  }

  private getOwners() {
    this.owners = this.ownersService.getOwners();
  }

  private getFarms() {
    this.farms = this.farmsService.getFarms();
  }

  private onUserChange(user: User) {
    const permissions = this.authenticationService.validatePermission(AppConfig.permissions.FARMS);
    this.farmPermission$.next(permissions);
  }

  private async validateActions() {
    if (!this.userSubscription) {
      this.userSubscription = (await this.authenticationService.getLoggedUser()).subscribe(user => this.onUserChange(user));
    }
  }

}
