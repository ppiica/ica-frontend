import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppConfig } from './../../../../config/app.config';
import { User, DefaultPermissions, Permission } from './../../../../shared/models/user';
import { LoggerService } from './../../../../shared/services/logger/logger.service';
import { AuthenticationService } from './../../../../shared/services/authentication/authentication.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Farm } from './../../../../shared/models/farms';
import { Observable } from 'rxjs/Observable';
import { FarmsService } from '../../shared/farms.service';
import { Router } from '@angular/router';
import { DeleteDialogComponent } from '../../../../shared/components/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-farms',
  templateUrl: './farms.page.html',
  styleUrls: ['./farms.page.css']
})
export class FarmsPageComponent implements OnInit {

  private userSubscription = null;
  public farms: Observable<Farm[]>;
  public farmPermission$: BehaviorSubject<Permission>;

  constructor(
    private farmsService: FarmsService,
    private router: Router,
    private matDialog: MatDialog,
    private authenticationService: AuthenticationService
  ) {
    this.farmPermission$ = new BehaviorSubject(DefaultPermissions);
  }

  ngOnInit() {
    this.farms = this.farmsService.getFarms();
    this.validateActions();
  }

  private onUserChange(user: User) {
    this.farmPermission$.next(this.authenticationService.validatePermission(AppConfig.permissions.FARMS));
  }

  private async validateActions() {
    if (!this.userSubscription) {
      this.userSubscription = (await this.authenticationService.getLoggedUser()).subscribe(user => this.onUserChange(user));
    }
  }

  public editFarm(farm: Farm) {
    this.farmsService.farmToEdit = farm;
    this.router.navigate(['admin', 'farms', 'form', farm.registerNumber]);
  }

  public removeFarm(farmRegisterNumber: string) {
    const dialog = this.matDialog.open(DeleteDialogComponent, {
      data: {
        removeItem: 'REMOVE_FARM'
      }
    });
    dialog.beforeClose().subscribe((result) => {
      if (result) {
        this.farmsService.removeFarm(farmRegisterNumber).subscribe();
      }
    });
  }

}
