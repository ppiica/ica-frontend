import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmsPageComponent } from './farms.page';

describe('FarmsPageComponent', () => {
  let component: FarmsPageComponent;
  let fixture: ComponentFixture<FarmsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
