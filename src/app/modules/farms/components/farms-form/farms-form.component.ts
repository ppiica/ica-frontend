import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import { Permission, DefaultPermissions } from './../../../../shared/models/user';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormGroup, Validators, ValidatorFn} from '@angular/forms';
import { Component, OnInit, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Farm } from '../../../../shared/models/farms';
import { Region } from '../../../../shared/models/regions';
import { Office } from '../../../../shared/models/office';
import { Owner } from '../../../../shared/models/owners';

@Component({
  selector: 'app-farm-form',
  templateUrl: './farms-form.component.html',
  styleUrls: ['./farms-form.component.css']
})
export class FarmFormComponent implements OnInit, OnDestroy {

  public farmForm: FormGroup;
  public title: string;
  public startDate = new Date();
  public farmsPermissions = DefaultPermissions;
  private farmsPermissionsSubscription: Subscription;
  private latitudeValidator: ValidatorFn;
  private longitudeValidator: ValidatorFn;
  private validatorRegisterNumber: ValidatorFn;
  private onlyNumbersValidator: ValidatorFn;
  private requiredMaxLengthValidatorName: ValidatorFn;

  @Input()
  public mode: string;

  @Input()
  public farm: Farm;

  @Input()
  public departments: Observable<Region[]>;

  @Input()
  public municipalities: Observable<Region[]>;

  @Input()
  public sidewalks: Observable<Region[]>;

  @Input()
  public farmsPermission$: BehaviorSubject<Permission>;

  @Input()
  public offices: Observable<Office[]>;

  @Input()
  public owners: Observable<Owner[]>;

  @Output()
  public submitCreate: EventEmitter<Farm>;

  @Output()
  public submitUpdate: EventEmitter<Farm>;

  @Output()
  public selectDepartment: EventEmitter<string>;

  @Output()
  public selectMunicipality: EventEmitter<string>;


  constructor(
    private formBuilder: FormBuilder
  ) {
    this.submitCreate = new EventEmitter();
    this.submitUpdate = new EventEmitter();
    this.selectDepartment = new EventEmitter();
    this.selectMunicipality = new EventEmitter();
    this.farmsPermissions = DefaultPermissions;
    this.initValidators();

  }
 
  private initValidators() {
    this.latitudeValidator = Validators.compose([Validators.required, Validators.max(90), Validators.min(-90)]);
    this.longitudeValidator = Validators.compose([Validators.required, Validators.max(180), Validators.min(-180)]);
    this.validatorRegisterNumber = Validators.compose([Validators.minLength(2), Validators.maxLength(11), Validators.required]);
    this.onlyNumbersValidator = Validators.pattern(new RegExp('^[1-9][0-9]*$'));
    this.requiredMaxLengthValidatorName = Validators.compose([Validators.minLength(2), Validators.maxLength(255), Validators.required]);
    
  }

  ngOnInit() {
    console.log(this.mode);
    this.title = this.mode === 'edit' ? 'EDIT_FARM' : 'CREATE_FARM';
    this.loadData();
    this.initForm(this.farm);
  }

  ngOnDestroy() {
    if (this.farmsPermissionsSubscription) {
      this.farmsPermissionsSubscription.unsubscribe();
    }
  }

  private initForm(farm?: Farm) {
    this.farmForm = this.formBuilder.group({
      registerNumber: [undefined, [this.validatorRegisterNumber, this.onlyNumbersValidator]],
      name: [undefined, this.requiredMaxLengthValidatorName],
      latitude: [undefined, this.latitudeValidator],
      longitude: [undefined, this.longitudeValidator],
      area: [undefined, Validators.required],
      department: ['', Validators.required],
      municipality: ['', Validators.required],
      sidewalk: ['', Validators.required],
      office: ['', Validators.required],
      owner: ['', Validators.required]
    });

    if (this.mode === 'edit') {
    }

    if (farm) {
      const farmSet = {
        registerNumber: farm.registerNumber ,
        name: farm.name ,
        latitude: farm.latitude,
        longitude: farm.longitude,
        area: farm.area,
        department: farm.sidewalk.municipality.department.id,
        municipality: farm.sidewalk.municipality.id,
        sidewalk: farm.sidewalkId,
        office: farm.officeId,
        owner: farm.ownerId
      };
      this.farmForm.setValue(farmSet);
    }
  }

  private prepareDataToSend(): Farm {
    const formValue = this.farmForm.value;
    const farm = {
      registerNumber: parseInt(formValue.registerNumber),
      officeId: formValue.office.toString(),
      sidewalkId: formValue.sidewalk.toString(),
      ownerId: formValue.owner.toString(),
      name: formValue.name,
      latitude: formValue.latitude,
      longitude: formValue.longitude,
      area: formValue.area,
    };
    return farm;
  }

  public submit() {
    const farm = this.prepareDataToSend();
    if (this.mode === 'create') {
      this.submitCreate.emit(farm);
    } else if (this.mode === 'edit') {
      this.submitUpdate.emit(farm);
    }
  }

  public onSelectDepartment() {
    this.selectDepartment.emit(this.farmForm.value.department);
  }

  public onSelectMunicipality() {
    this.selectMunicipality.emit(this.farmForm.value.municipality);
  }
  private loadData() {
    this.farmsPermissionsSubscription = this.farmsPermission$.subscribe((permissions: Permission) => {
      this.farmsPermissions = permissions;
    });
  }
}
