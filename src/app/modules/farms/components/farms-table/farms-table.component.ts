import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Permission, DefaultPermissions } from './../../../../shared/models/user';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Farm } from '../../../../shared/models/farms';
import { Subscription } from 'rxjs/Subscription';
import { } from 'events';

@Component({
  selector: 'app-farms-table',
  templateUrl: './farms-table.component.html',
  styleUrls: ['./farms-table.component.css']
})
export class FarmsTableComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  @ViewChild('input') input: ElementRef;

  public farmsDataSource: MatTableDataSource<Farm>;

  private farmsSubscription: Subscription;

  public farmsPermissions: Permission;

  private farmsPermissionsSubscription: Subscription;

  public displayedColumns = ['registerNumber', 'name', 'latitude', 'longitude', 'actions'];

  @Input()
  public farmsObservable: Observable<Farm[]>;

  @Input()
  public farmsPermission$: BehaviorSubject<Permission>;

  @Output()
  public clickEditFarm: EventEmitter<Farm>;

  @Output()
  public clickRemoveFarm: EventEmitter<Farm>;

  constructor() {
    this.clickEditFarm = new EventEmitter();
    this.clickRemoveFarm = new EventEmitter();
    this.farmsPermissions = DefaultPermissions;
  }

  ngOnInit() {
    this.farmsDataSource = new MatTableDataSource([]);
    this.loadData();
  }

  ngAfterViewInit() {
    this.farmsDataSource.paginator = this.paginator;
    this.farmsDataSource.sort = this.sort;
  }

  ngOnDestroy() {
    if (this.farmsSubscription) {
      this.farmsSubscription.unsubscribe();
    }
    if (this.farmsPermissionsSubscription) {
      this.farmsPermissionsSubscription.unsubscribe();
    }
  }

  public applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.farmsDataSource.filter = filterValue;
  }

  private loadData() {
    this.farmsSubscription = this.farmsObservable.subscribe((farms) => {
      this.farmsDataSource.data = farms;
    });
    this.farmsPermissionsSubscription = this.farmsPermission$.subscribe((permission: Permission) => {
      this.farmsPermissions = permission;
    });
  }

}
