import { AuthenticationService } from './../../shared/services/authentication/authentication.service';
import { FarmsService } from './shared/farms.service';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from './../../material.module';
import { FarmsPageComponent } from './pages/farms/farms.page';
import { FarmsRoutingModule } from './farms-routing.module';
import { FarmsTableComponent } from './components/farms-table/farms-table.component';
import { FarmFormComponent } from './components/farms-form/farms-form.component';
import { FarmFormPageComponent } from './pages/farm-form/farm-form.page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    MaterialModule,
    FarmsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    FlexLayoutModule
  ],
  exports: [],
  declarations: [
    FarmsPageComponent,
    FarmsTableComponent,
    FarmFormComponent,
    FarmFormPageComponent
  ],
  providers: [
    FarmsService,
    AuthenticationService
  ],
})
export class FarmsModule { }
