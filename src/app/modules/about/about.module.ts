import { MaterialModule } from './../../material.module';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AboutPageComponent } from './pages/about/about.page';
import { AboutRoutingModule } from './about-routing.module';
import { IcaAboutComponent } from './components/ica-about/ica-about.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    AboutRoutingModule,
    MaterialModule
  ],
  exports: [],
  declarations: [
    AboutPageComponent,
    IcaAboutComponent
  ],
  providers: [],
})
export class AboutModule { }
