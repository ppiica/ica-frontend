import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OnlineHelpPageComponent } from './pages/onlinehelp/onlinehelp.page';

const routes: Routes = [
  { path: '', component: OnlineHelpPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OnlineHelpRoutingModule { }

export const routedComponents = [OnlineHelpPageComponent];
