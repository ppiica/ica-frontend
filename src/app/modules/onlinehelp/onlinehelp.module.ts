import { MaterialModule } from './../../material.module';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { OnlineHelpPageComponent } from './pages/onlinehelp/onlinehelp.page';
import { OnlineHelpRoutingModule } from './onlinehelp-routing.module';
import { IcaOnlineHelpComponent } from './components/ica-onlinehelp/ica-onlinehelp.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    OnlineHelpRoutingModule,
    MaterialModule
  ],
  exports: [],
  declarations: [
    OnlineHelpPageComponent,
    IcaOnlineHelpComponent
  ],
  providers: [],
})
export class OnlineHelpModule { }
