import { AppConfig } from './../../config/app.config';
import { AuthGuardService } from './../../shared/services/authentication/auth-guard.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OwnersPageComponent } from './pages/owners/owners.page';
import { OwnerFormPageComponent } from './pages/owner-form/owner-form.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
    data: {formName: AppConfig.permissions.OWNERS , action: AppConfig.actions.VIEW }
  },
  {
    path: 'list',
    component: OwnersPageComponent,
    canActivate: [AuthGuardService],
    data: {formName: AppConfig.permissions.OWNERS , action: AppConfig.actions.VIEW }
  },
  {
    path: 'form/:id',
    component: OwnerFormPageComponent,
    canActivate: [AuthGuardService],
    data: {formName: AppConfig.permissions.OWNERS , action: AppConfig.actions.CREATE }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OwnersRoutingModule { }

export const routedComponents = [OwnersPageComponent, OwnerFormPageComponent];
