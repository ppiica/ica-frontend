import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Owner } from './../../shared/models/owners';
import { AppConfig } from '../../config/app.config';
import { LoggerService } from '../../shared/services/logger/logger.service';
import { UtilService } from '../../shared/services/util/util.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class OwnersService {

  private owners$: BehaviorSubject<Owner[]>;

  public ownerToEdit: Owner;

  constructor(
    private http: HttpClient,
    private utils: UtilService
  ) {
    this.owners$ = new BehaviorSubject<Owner[]>([]);
  }


  public getOwners(): Observable<Owner[]> {
    this.http.get<Owner[]>(AppConfig.endpoints.owners).subscribe((owners) => {
      this.owners$.next(owners);
    });
    return this.owners$.asObservable();
  }

  public getOwner(ownerId: string) {
    return this.http.get<Owner>(AppConfig.endpoints.owners + '/' + ownerId);
  }

  public createOwner(owner: Owner): Observable<Owner> {
    return this.http.post<Owner>(AppConfig.endpoints.owners, owner, httpOptions)
      .pipe(
        tap((ownerCreated) => {
          this.getOwnersAfterThat();
          LoggerService.log(`owner created is=${JSON.stringify(ownerCreated)}`);
          this.utils.showSnackBar('OWNER_CREATED');
        }),
        catchError(this.handleError<Owner>('create owner'))
      );
  }

  public updateOwner(owner: Owner): Observable<Owner> {
    return this.http.put<Owner>(AppConfig.endpoints.owners + '/' + owner.id, owner, httpOptions)
      .pipe(
        tap((ownerUpdated) => {
          this.getOwnersAfterThat();
          LoggerService.log(`owner updated is=${JSON.stringify(ownerUpdated)}`);
          this.utils.showSnackBar('OWNER_UPDATED');
        })
      );
  }

  public removeOwner(ownerId: string): Observable<any> {
    return this.http.delete<any>(AppConfig.endpoints.owners + '/' + ownerId)
      .pipe(
        tap(() => {
          this.getOwnersAfterThat();
          LoggerService.log(`owner removed`);
          this.utils.showSnackBar('OWNER_REMOVED');
        })
      );
  }

  private getOwnersAfterThat() {
    this.getOwners();
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      LoggerService.log(`${operation} failed: ${error.message}`);

      switch (error.status) {
        case 500:
          this.utils.showSnackBar('ERROR_500');
          break;
        case 400:
          this.utils.showSnackBar('OWNER_ALREADY_EXISTS');
          break;
        default:
          break;
      }


      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
