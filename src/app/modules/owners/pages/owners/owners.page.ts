import { AppConfig } from './../../../../config/app.config';
import { Permission, DefaultPermissions, User } from './../../../../shared/models/user';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import { AuthenticationService } from './../../../../shared/services/authentication/authentication.service';
import { MatDialog } from '@angular/material';
import { DeleteDialogComponent } from '../../../../shared/components/delete-dialog/delete-dialog.component';
import { Router, NavigationExtras } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { OwnersService } from './../../owners.service';
import { Owner } from '../../../../shared/models/owners';

@Component({
  selector: 'app-owners',
  templateUrl: './owners.page.html',
  styleUrls: ['./owners.page.css']
})
export class OwnersPageComponent implements OnInit, OnDestroy {

  public owners: Observable<Owner[]>;
  private userSubscription: Subscription;
  public ownersPermission$: BehaviorSubject<Permission>;

  constructor(
    private ownersService: OwnersService,
    private matDialog: MatDialog,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.ownersPermission$ = new BehaviorSubject(DefaultPermissions);
  }

  ngOnInit() {
    this.owners = this.ownersService.getOwners();
    this.validateActions();
  }

  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }

  public editOwner(owner: Owner) {
    this.ownersService.ownerToEdit = owner;
    this.router.navigate(['admin', 'owners', 'form', owner.id]);
  }

  public removeOwner(ownerId: string) {
    const dialog = this.matDialog.open(DeleteDialogComponent, {
      data: {
        removeItem: 'REMOVE_OWNER'
      }
    });
    dialog.beforeClose().subscribe((result) => {
      if (result) {
        this.ownersService.removeOwner(ownerId).subscribe();
      }
    });
  }

  private onUserChange(user: User) {
    this.ownersPermission$.next(this.authenticationService.validatePermission(AppConfig.permissions.OWNERS));
  }

  private async validateActions() {
    if (!this.userSubscription) {
      this.userSubscription = (await this.authenticationService.getLoggedUser()).subscribe((user: User) => this.onUserChange(user));
    }
  }

}
