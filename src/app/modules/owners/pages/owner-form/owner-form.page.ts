import { AppConfig } from './../../../../config/app.config';
import { AuthenticationService } from './../../../../shared/services/authentication/authentication.service';
import { Permission, DefaultPermissions, User } from './../../../../shared/models/user';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router, ActivatedRoute } from '@angular/router';

import { OwnersService } from './../../owners.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Owner } from '../../../../shared/models/owners';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-owner-form-page',
  templateUrl: './owner-form.page.html',
  styleUrls: ['./owner-form.page.css']
})
export class OwnerFormPageComponent implements OnInit, OnDestroy {

  public mode = 'edit';
  public ownerData: Owner;
  private userSubscription: Subscription;
  public ownersPermission$: BehaviorSubject<Permission>;

  constructor(
    private ownersService: OwnersService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService
  ) {
    this.ownersPermission$ = new BehaviorSubject(DefaultPermissions);
  }

  ngOnInit() {
    this.getOwner();
    this.validateActions();
  }

  ngOnDestroy() {
    delete this.ownersService.ownerToEdit;
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }

  public createOwner(owner: Owner) {
    this.ownersService.createOwner(owner).subscribe((res) => {
      this.router.navigate(['admin', 'owners', 'list']);
    });
  }

  public updateOwner(owner: Owner) {
    this.ownersService.updateOwner(owner).subscribe((res) => {
      this.router.navigate(['admin', 'owners', 'list']);
    });
  }

  private getOwner() {
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params.mode) {
        this.mode = params.mode;
      }
      if (this.ownersService.ownerToEdit) {
        this.ownerData = this.ownersService.ownerToEdit;
      } else if (this.mode !== 'create' && params.id ) {
        this.ownersService.getOwner(params.id).subscribe((owner) => {
          this.ownerData = owner;
        });
      }
    });
  }

  private onUserChange(user: User) {
    this.ownersPermission$.next(this.authenticationService.validatePermission(AppConfig.permissions.OWNERS));
  }

  private async validateActions() {
    if (!this.userSubscription) {
      this.userSubscription = (await this.authenticationService.getLoggedUser()).subscribe((user: User) => this.onUserChange(user));
    }
  }

}
