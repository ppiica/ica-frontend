import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnerFormPageComponent } from './owner-form.page';

describe('OwnerFormPageComponent', () => {
  let component: OwnerFormPageComponent;
  let fixture: ComponentFixture<OwnerFormPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnerFormPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerFormPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
