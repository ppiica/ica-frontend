import { AuthenticationService } from './../../shared/services/authentication/authentication.service';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { MaterialModule } from './../../material.module';
import { OwnersRoutingModule } from './owners-routing.module';
import { OwnersPageComponent } from './pages/owners/owners.page';
import { OwnersTableComponent } from './components/owners-table/owners-table.component';
import { OwnerDetailComponent } from './components/owner-detail/owner-detail.component';
import { OwnersService } from './owners.service';
import { OwnerFormComponent } from './components/owner-form/owner-form.component';
import { OwnerFormPageComponent } from './pages/owner-form/owner-form.page';
import { SharedModule } from '../../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    OwnersPageComponent,
    OwnersTableComponent,
    OwnerDetailComponent,
    OwnerFormComponent,
    OwnerFormPageComponent
  ],
  imports: [
    CommonModule,
    OwnersRoutingModule,
    TranslateModule.forChild(),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    FlexLayoutModule
  ],
  exports: [],
  providers: [
    OwnersService,
    AuthenticationService
  ],
})
export class OwnersModule { }
