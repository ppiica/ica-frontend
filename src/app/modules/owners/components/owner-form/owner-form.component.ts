import { Permission, DefaultPermissions } from './../../../../shared/models/user';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { UtilService } from './../../../../shared/services/util/util.service';
import { FormBuilder, FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { MAT_CHECKBOX_CLICK_ACTION } from '@angular/material';
import { AbstractControl } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { Owner } from '../../../../shared/models/owners';

@Component({
  selector: 'app-owner-form',
  templateUrl: './owner-form.component.html',
  styleUrls: ['./owner-form.component.css']
})
export class OwnerFormComponent implements OnInit, OnDestroy {

  public ownerForm: FormGroup;
  public title: string;
  private requiredMaxLengthValidator: ValidatorFn;
  private optionalMaxLengthValidator: ValidatorFn;
  private emailValidator: ValidatorFn;
  private ownersPermissions: Permission;
  private ownersPermissonsSubscription: Subscription;
  private idNumberValidator: ValidatorFn;
  private onlyLettersValidator: ValidatorFn;
  private requiredMaxLengthValidatorName: ValidatorFn;
  private validatorPhone: ValidatorFn;
  private validatorAddress: ValidatorFn;

  @Input()
  public mode: string;

  @Input()
  public ownersPermission$: BehaviorSubject<Permission>;

  @Input()
  public owner: Owner;

  @Output()
  public submitCreate: EventEmitter<Owner>;

  @Output()
  public submitUpdate: EventEmitter<Owner>;

  constructor(
    private formBuilder: FormBuilder,
  ) {
    this.submitCreate = new EventEmitter();
    this.submitUpdate = new EventEmitter();
    this.ownersPermissions = DefaultPermissions;
    this.initValidators();
  }

  ngOnInit() {
    console.log(this.mode);
    this.title = this.mode === 'edit' ? 'EDIT_OWNER' : 'CREATE_OWNER';
    this.initForm(this.owner);
    this.validatePermissions();
  }

  ngOnDestroy() {
    if (this.ownersPermissonsSubscription) {
      this.ownersPermissonsSubscription.unsubscribe();
    }
  }

  public submit() {
    const owner = this.prepareDataToSend();
    if (this.mode === 'create') {
      this.submitCreate.emit(owner);
    } else if (this.mode === 'edit') {
      this.submitUpdate.emit(owner);
    }
  }

  private initValidators() {
    this.requiredMaxLengthValidator = Validators.compose([Validators.minLength(2), Validators.maxLength(11), Validators.required]);
    this.optionalMaxLengthValidator = Validators.compose([Validators.minLength(2), Validators.maxLength(45)]);
    this.emailValidator = Validators.compose([Validators.email, Validators.required, Validators.minLength(2), Validators.maxLength(254)]);
    this.idNumberValidator = Validators.pattern('^[1-9][0-9]*$');
    this.onlyLettersValidator = Validators.pattern('^[a-zA-Z áéíóúÁÉÍÓÚñÑ]*$');
    this.requiredMaxLengthValidatorName = Validators.compose([Validators.minLength(2), Validators.maxLength(45), Validators.required]);
    this.validatorPhone = Validators.compose([Validators.minLength(7), Validators.maxLength(11)]);
    this.validatorAddress = Validators.compose([Validators.minLength(2), Validators.maxLength(100)]);
  }

  private initForm(owner?: Owner) {

    const groupConfig = {
      id: ['', [this.requiredMaxLengthValidator, this.idNumberValidator]],
      firstName: ['', [this.requiredMaxLengthValidatorName, this.onlyLettersValidator]],
      lastNameOne: ['', [this.requiredMaxLengthValidatorName, this.onlyLettersValidator]],
      lastNameTwo: ['', [this.optionalMaxLengthValidator, this.onlyLettersValidator]],
      phone: [undefined, [this.validatorPhone, Validators.pattern('^[0-9]*$')]],
      email: ['', this.emailValidator],
      address: ['', this.validatorAddress]
    };

    this.ownerForm = this.formBuilder.group(groupConfig);
    if (owner) {
      const ownerSet = {
        id: owner.id,
        firstName: owner.firstName,
        lastNameOne: owner.lastNameOne,
        lastNameTwo: owner.lastNameTwo,
        phone: owner.phone,
        email: owner.email,
        address: owner.address
      };
      this.ownerForm.setValue(ownerSet);
    }
  }

  private prepareDataToSend(): Owner {
    const formValue = this.ownerForm.value;
    const owner = {
      id: formValue.id.toString(),
      firstName: formValue.firstName,
      lastNameOne: formValue.lastNameOne,
      lastNameTwo: formValue.lastNameTwo,
      phone: parseInt(formValue.phone),
      email: formValue.email,
      address: formValue.address
    };
    if(formValue.lastNameTwo == null || formValue.lastNameTwo == undefined){
      delete owner.lastNameTwo;
    }
    if(formValue.address == null || formValue.address == undefined){
      delete owner.address;
    }
    return owner;
  }

  private validatePermissions() {
    if (!this.ownersPermissonsSubscription) {
      this.ownersPermissonsSubscription = this.ownersPermission$.subscribe((permissions: Permission) => {
        this.ownersPermissions = permissions;
      });
    }
  }
}

