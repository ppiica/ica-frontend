import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Permission } from './../../../../shared/models/user';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { debounceTime, distinctUntilChanged, startWith, tap, delay } from 'rxjs/operators';
import { merge } from 'rxjs/observable/merge';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { Component, OnInit, ViewChild, AfterViewInit, Input, Output, EventEmitter, OnDestroy, ElementRef } from '@angular/core';

import { Owner } from '../../../../shared/models/owners';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-owners-table',
  templateUrl: './owners-table.component.html',
  styleUrls: ['./owners-table.component.css']
})
export class OwnersTableComponent implements OnInit, AfterViewInit, OnDestroy {


  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  @ViewChild('input') input: ElementRef;

  private ownersPermissions: Permission;
  private ownersPermissonsSubscription: Subscription;
  public ownerDataSource: MatTableDataSource<Owner>;
  private ownersSubscription: Subscription;

  public displayedColumns = ['id', 'firstName', 'phone', 'email', 'address', 'actions'];

  @Input()
  public ownersPermission$: BehaviorSubject<Permission>;

  @Input()
  public ownersObservable: Observable<Owner[]>;

  @Output()
  public clickEditOwner: EventEmitter<Owner>;

  @Output()
  public clickRemoveOwner: EventEmitter<Owner>;

  constructor() {
    this.clickEditOwner = new EventEmitter();
    this.clickRemoveOwner = new EventEmitter();
  }

  ngOnInit() {
    this.ownerDataSource = new MatTableDataSource([]);
    this.loadData();
    this.validatePermissions();
  }

  ngAfterViewInit() {
    this.ownerDataSource.paginator = this.paginator;
    this.ownerDataSource.sort = this.sort;
  }

  ngOnDestroy() {
    if (this.ownersSubscription) {
      this.ownersSubscription.unsubscribe();
    }
    if (this.ownersPermissonsSubscription) {
      this.ownersPermissonsSubscription.unsubscribe();
    }
  }

  public applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.ownerDataSource.filter = filterValue;
  }

  private loadData() {
    this.ownersSubscription = this.ownersObservable.subscribe((owners) => {
      this.ownerDataSource.data = owners;
    });
  }

  private validatePermissions() {
    if (!this.ownersPermissonsSubscription) {
      this.ownersPermissonsSubscription = this.ownersPermission$.subscribe((permissions: Permission) => {
        this.ownersPermissions = permissions;
      });
    }
  }
}
