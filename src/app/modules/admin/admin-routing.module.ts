import { AppConfig } from './../../config/app.config';
import { AuthGuardService } from './../../shared/services/authentication/auth-guard.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminPageComponent } from './pages/admin/admin.page';

const routes: Routes = [
  {
    path: '',
    component: AdminPageComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: '../dashboard/dashboard.module#DashboardModule',
        canActivate: [AuthGuardService],
        data: {formName: AppConfig.permissions.ANY , action: AppConfig.actions.VIEW }
      },
      {
        path: 'users',
        loadChildren: '../users/users.module#UsersModule',
        canActivate: [AuthGuardService],
        data: {formName: AppConfig.permissions.USERS , action: AppConfig.actions.VIEW }
      },
      {
        path: 'farms',
        loadChildren: '../farms/farms.module#FarmsModule',
        canActivate: [AuthGuardService],
        data: {formName: AppConfig.permissions.FARMS , action: AppConfig.actions.VIEW }
      },
      {
        path: 'visits',
        loadChildren: '../visits/visits.module#VisitsModule',
        canActivate: [AuthGuardService],
        data: {formName: AppConfig.permissions.VISITS , action: AppConfig.actions.VIEW }
      },
      {
        path: 'owners',
        loadChildren: '../owners/owners.module#OwnersModule',
        data: {formName: AppConfig.permissions.OWNERS , action: AppConfig.actions.VIEW }
      },
      {
        path: 'onlinehelp',
        loadChildren: '../onlinehelp/onlinehelp.module#OnlineHelpModule',
        data: {formName: AppConfig.permissions.ANY , action: AppConfig.actions.VIEW }
      },
      {
        path: 'about',
        loadChildren: '../about/about.module#AboutModule',
        data: {formName: AppConfig.permissions.ANY , action: AppConfig.actions.VIEW }
      },
      {
        path: 'offices',
        loadChildren: '../offices/offices.module#OfficesModule',
        canActivate: [AuthGuardService],
        data: {formName: AppConfig.permissions.CRUDS , action: AppConfig.actions.VIEW }
      }
    ],
    data: {formName: AppConfig.permissions.ANY , action: AppConfig.actions.VIEW }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule { }

export const routedComponents = [AdminPageComponent];
