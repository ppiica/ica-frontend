import { AppConfig } from './../../../../config/app.config';
import { User } from './../../../../shared/models/user';
import { Subscription } from 'rxjs/Subscription';
import { LoggerService } from './../../../../shared/services/logger/logger.service';
import { AuthenticationService } from './../../../../shared/services/authentication/authentication.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {

  @Output()
  public clickLogout: EventEmitter<any> = new EventEmitter();

  private userSubcription: Subscription = null;
  private _listBase = [
    {
      name: 'HOME',
      icon: 'home',
      formName: AppConfig.permissions.ANY,
      routerLink: ''
    }, {
      name: 'USERS',
      icon: 'people',
      formName: AppConfig.permissions.USERS,
      routerLink: 'users'
    }, {
      name: 'OFFICES',
      icon: 'store',
      formName: AppConfig.permissions.CRUDS,
      routerLink: 'offices'
    }, {
      name: 'OWNERS',
      icon: 'persons',
      formName: AppConfig.permissions.OWNERS,
      routerLink: 'owners'
    }, {
      name: 'FARMS',
      icon: 'terrain',
      formName: AppConfig.permissions.FARMS,
      routerLink: 'farms'
    }, {
      name: 'VISITS',
      icon: 'receipt',
      formName: AppConfig.permissions.VISITS,
      routerLink: 'visits'
    }, {
      name: 'ONLINE_HELP',
      icon: 'live_help',
      formName: AppConfig.permissions.ANY,
      routerLink: 'onlinehelp'
    }, {
      name: 'ABOUT',
      icon: 'contacts',
      formName: AppConfig.permissions.ANY,
      routerLink: 'about'
    }];

    public isOpen = true;
    public list: any[] = [];

    constructor(private authenticationService: AuthenticationService) { }

    ngOnInit() {
      this.initList();
    }

    private onUserChange(user: User) {
      this.list.length = 0;
      if (user) {
        console.log(user.UserRoleForm.map(form => form.form.description));
        this._listBase.forEach(route => {
          if (route.formName === AppConfig.permissions.ANY || user.UserRoleForm.find(p => p.form.description === route.formName)) {
            this.list.push(route);
          }
        });
      }
    }

    private async initList() {
      this.list = [];
      try {
        if (!this.userSubcription) {
          this.userSubcription = (await this.authenticationService.getLoggedUser()).subscribe(user => this.onUserChange(user));
        }
      } catch (exception) {
        LoggerService.error(exception);
      }
    }

    /**
    * onClickLogout
    */
    public onClickLogout() {
      this.clickLogout.emit(true);
    }

  }
