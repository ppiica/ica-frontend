import { AuthenticationService } from './../../../../shared/services/authentication/authentication.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.css']
})
export class AdminPageComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
  }

  /**
   * onClickLogout
   */
  public onClickLogout() {
    this.authenticationService.logout();
  }


}
