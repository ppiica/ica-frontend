import { AuthenticationService } from './../../shared/services/authentication/authentication.service';

import { AuthGuardService } from './../../shared/services/authentication/auth-guard.service';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { AdminPageComponent } from './pages/admin/admin.page';
import { AdminRoutingModule } from './admin-routing.module';
import { MaterialModule } from '../../material.module';
import { SidenavComponent } from './components/sidenav/sidenav.component';

@NgModule({
  imports: [
    AdminRoutingModule,
    CommonModule,
    MaterialModule,
    TranslateModule.forChild()
  ],
  exports: [RouterModule],
  declarations: [
    SidenavComponent,
    AdminPageComponent
  ],
  providers: [
    AuthGuardService,
    AuthenticationService
  ],
})
export class AdminModule { }
