import { AuthGuardService } from './../../shared/services/authentication/auth-guard.service';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoreRoutingModule } from './core-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CoreRoutingModule
  ],
  declarations: [],
  exports: [
    RouterModule
  ],
  providers: [
    AuthGuardService
  ]
})
export class CoreModule { }
