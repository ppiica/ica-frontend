import { AppConfig } from './../../config/app.config';
import { AuthGuardService } from './../../shared/services/authentication/auth-guard.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'admin/dashboard',
    pathMatch: 'full',
    data: {formName: AppConfig.permissions.ANY , action: AppConfig.actions.VIEW }
  },
  {
    path: 'login',
    loadChildren: '../login/login.module#LoginModule',
    data: {formName: AppConfig.permissions.ANY , action: AppConfig.actions.VIEW }
  },
  {
    path: 'admin',
    loadChildren: '../admin/admin.module#AdminModule',
    canActivate: [AuthGuardService],
    data: {formName: AppConfig.permissions.ANY , action: AppConfig.actions.VIEW }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class CoreRoutingModule { }

// export const routedComponents = [CoreComponent];
