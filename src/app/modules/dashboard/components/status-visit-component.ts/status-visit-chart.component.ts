import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { VisitsService } from '../../../visits/shared/visits.service';
import { TranslateService } from '@ngx-translate/core';
import { stat } from 'fs';

@Component({
  selector: 'status-visit-chart',
  templateUrl: './status-visit-chart.component.html',
  styleUrls: ['./status-visit-chart.component.css']
})
export class StatusVisitChartComponent {

  public data: any[];
  view: any[] = [700, 500];

  colorScheme = {
    domain: ['#3368FF', '#5AA454', '#C7B42C','#FF5733', '#C70039', '#AAAAAA']
  };

  constructor(
    private visitsService: VisitsService,
    private translate: TranslateService) {

    this.getTotalVisitByState();
  }

  onSelect(event) {
    console.log(event);
  }

  public async getTotalVisitByState() {
    this.data = await this.visitsService.getTotalVisitByStateChart();
    if(this.data.length > 0){
      this.translate.get(this.data.map(value => value.name)).subscribe((values) => {
        this.data.forEach((status) => {
          status.name = values[status.name];
          status.value = parseInt(status.value);
        });
      });
    }
  }
}