
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { IcaDashboardComponent } from './ica-dashboard.component';

describe('IcaDashboardComponent', () => {
  let component: IcaDashboardComponent;
  let fixture: ComponentFixture<IcaDashboardComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ IcaDashboardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IcaDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
