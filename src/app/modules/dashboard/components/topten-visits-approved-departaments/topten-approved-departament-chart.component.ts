import { Component, NgModule, Input } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { Observable } from 'rxjs/Observable';
import { VisitsService } from '../../../visits/shared/visits.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'topten-approved-visit-departament',
  templateUrl: './topten-approved-departament-chart.component.html',
  styleUrls: ['./topten-approved-departament-chart.component.css']
})
export class TopTenApprovedDepartamentsChartComponent {

  public data: any[];
  view: any[] = [700, 500];

  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  legendTitle: any;
  showXAxisLabel = true;
  xAxisLabel: any;
  showYAxisLabel = true;
  yAxisLabel: any;
  barPadding = 2;
  groupPadding = 10;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#FFC300', '#BD979D', '#2ECC71', '#FF5733', '#C70039', '#900C3F', '#229954', '#884EA0', '#935116', '#AAAAAA']
  };

  constructor(private visitsService: VisitsService, private translate: TranslateService) {
    this.getVisitApproved();
    this.translate.get(['LEGEND_TITLE_DEPARTAMENT', 'LEGEND_TITLE_FARMS_APPROVED', 'LEGEND_TITLE_MONTHS'])
      .subscribe((values) => {
        this.legendTitle = values.LEGEND_TITLE_DEPARTAMENT;
        this.xAxisLabel = values.LEGEND_TITLE_MONTHS;
        this.yAxisLabel = values.LEGEND_TITLE_FARMS_APPROVED;
      });
  }

  onSelect(event) {
    console.log(event);
  }

  public async getVisitApproved() {
    this.data = await this.visitsService.getVisitApprovedChart();
    if (this.data.length > 0) {
      this.translate.get(this.data.map(value => value.name)).subscribe((values) => {
        this.data.forEach((item) => {
          item.name = values[item.name];
        });
      });
    }
  }
}