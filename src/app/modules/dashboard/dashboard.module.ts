import { MaterialModule } from './../../material.module';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DashboardPageComponent } from './pages/dashboard/dashboard.page';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { IcaDashboardComponent } from './components/ica-dashboard/ica-dashboard.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TopTenApprovedDepartamentsChartComponent } from './components/topten-visits-approved-departaments/topten-approved-departament-chart.component';
import { StatusVisitChartComponent } from './components/status-visit-component.ts/status-visit-chart.component';
import { VisitsService } from '../visits/shared/visits.service';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    DashboardRoutingModule,
    MaterialModule,
    NgxChartsModule
  ],
  exports: [],
  declarations: [
    DashboardPageComponent,
    IcaDashboardComponent,
    TopTenApprovedDepartamentsChartComponent,
    StatusVisitChartComponent
  ],
  providers: [VisitsService],
})
export class DashboardModule { }
